#include <Arduino.h>
#include <unity.h>

#include "modeluydu.hpp"
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>

ModelUydu modelUdyu;
Adafruit_BNO055 bno;

void setUp()
{
    modelUdyu.setSensor(&bno,GaziUzay::G_BNO055);
}

void test_bno_connection()
{
    TEST_ASSERT_EQUAL(GaziUzay::Sensor_State_t::Connecting,modelUdyu.getG_BNO().sensorState);
}

void test_bno_name()
{
    TEST_ASSERT_EQUAL_STRING("BNO055",modelUdyu.getG_BNO().name);
}

void test_bno_type()
{
    TEST_ASSERT_EQUAL_INT(GaziUzay::Sensor_Type_t::G_BNO055,modelUdyu.getG_BNO().type);
}

// void test_bno_adress()
// {
//     TEST_ASSERT_EQUAL_MEMORY(&bno,modelUdyu.getG_BNO().bno,sizeof(bno));
// }


void setup()
{
    delay(2000);
    UNITY_BEGIN();
    RUN_TEST(test_bno_connection);
    RUN_TEST(test_bno_name);
    RUN_TEST(test_bno_type);
    // RUN_TEST(test_bno_adress);
    UNITY_END();
}

void loop()
{

}