#include <Arduino.h>
#include <unity.h>

#include "Entity.hpp"
#include "modeluydu.hpp"

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

ModelUydu modelUdyu;
Adafruit_BME280 bme;

void setUp()
{
    modelUdyu.set_pressure();
    modelUdyu.set_altitude();
    modelUdyu.set_velocity();
}

void test_bme_connection()
{
    TEST_ASSERT_EQUAL_UINT8(GaziUzay::Sensor_State_t::Connecting,modelUdyu.getG_BME().sensorState);
}

void test_bme_name()
{
    TEST_ASSERT_EQUAL_STRING("BME280",modelUdyu.getG_BME().name);
}

void test_bme_type()
{
    TEST_ASSERT_EQUAL_UINT8(GaziUzay::Sensor_Type_t::G_BME280,modelUdyu.getG_BME().type);
}

void setup()
{
    delay(2000);
    modelUdyu.setSensor(&bme,GaziUzay::G_BME280);
    UNITY_BEGIN();
    RUN_TEST(test_bme_connection);
    RUN_TEST(test_bme_name);
    RUN_TEST(test_bme_type);
    UNITY_END();
}

void loop()
{

}