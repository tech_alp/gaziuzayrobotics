#include <Arduino.h>
#include <TaskScheduler.h>
#include "RGBLed.h"
#include "Notify.h"

Scheduler ts;

RGBLed led(3,5,6);
Notify notify;

void updateLed();
Task tLedUpdate(50,-1,&updateLed,&ts,true);

void updateLed() {
    led.update();
}

void updateSerial();
Task tSerialUpdate(100,-1,&updateSerial,&ts,false);

void updateSerial() {
    if(Serial.available()) {
        char ch = Serial.read();
        if(ch == '0') {
            notify.flags.initialising = false;
            notify.flags.armed = true;
            Serial.println("ARMED_NO_GPS");
        }
        if(ch == '1') {
            notify.flags.gps_num_sats = 5;
            Serial.println("ARMED_GPS");
        }
        if(ch == '2') {
            notify.flags.failsafe_battery = 1;
            Serial.println("LOW_BATTERY");
        }
        if(ch == '3') {
            notify.flags.failsafe_battery = 0;
            Serial.println("LOW_BATTERY deactivated");
        }
        if(ch == '4') {
            notify.flags.armed = false;
            notify.flags.gps_num_sats = 1;
            Serial.println("READY_NO_GPS");
        }
        if(ch == '5') {
            notify.flags.armed = false;
            Serial.println("DISARM");
        }
        if(ch == '6') {
            notify.flags.error = true;
            Serial.println("ERROR");
        }
        if(ch == '7') {
            notify.flags.error = false;
            Serial.println("ERROR DEACTIVATED");
        }
    }
}

void setup() {
    notify.init();
    notify.flags.initialising = true;
    Serial.begin(9600);
    tSerialUpdate.enableDelayed(100);
}


void loop() {
    ts.execute();
}
