#include <Arduino.h>
#include "motor.h"
#include "config.h"
#include "gpio.h"

constexpr int pot_pin = A2;

Motor motor(HAL::Pin::MOTOR_PIN,MIN_PWM_VALUE,MAX_PWM_VALUE);

uint16_t analogVal;

// Forward decl.
void serialEvent();

void setup() {
    Serial.begin(9600);
    motor.init();
    pinMode(pot_pin,INPUT);
}

uint32_t prev_time;

void loop() {
    serialEvent();

    if(millis() > prev_time + 100) {
        analogVal = analogRead(pot_pin);
        int val = map(analogVal,0,1023,MOTOR_MIN_SPEED,MOTOR_MAX_SPEED);
        motor.setSpeed(val);
        motor.output();
        prev_time = millis();
        Serial.println(val);
    }
}

void serialEvent() {
    while(Serial.available()) {
        char c = Serial.read();
        if(c=='1') {
            motor.armed(true);
            Serial.println("armed");
        }
        else if(c=='2') {
            motor.armed(false);
            Serial.println("disarmed");
        }
    }
}