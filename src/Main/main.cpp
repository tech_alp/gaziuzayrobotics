/////////////////////////////////////////////////////////////////
// Gazi Uzay Fligth Software for Türksat Model Uydu.           //
// Enes Alp 2020                                               //
// More at https://gitlab.com/tech_alp/gaziuzay                //
/////////////////////////////////////////////////////////////////

#include <Arduino.h>
#include "config.h"

#include <TimerOne.h>
#include "gaziuzay.hpp"
#include "modeluydu.hpp"

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <Adafruit_BME280.h>
#include <utility/imumaths.h>
#include <Servo.h>
#include <SoftwareSerial.h>

#include "serialmanager.h"
#include "seperation.h"

SerialManager<HardwareSerial> usb(&Serial);

ServoSeperation seperator(HAL::Pin::SEPERATION_SERVO_PIN);
RelaySeperation relay(HAL::Pin::FIRE_SMOKE_PIN);

// ----------------------------------------------------------------------------
uint8_t transmitingData[MAX_LEN_TRANSMITING_DATA];
volatile uint8_t m_packetBuffer[MAX_LEN_INCOMING_DATA];
volatile uint8_t m_packetBufferInd;
volatile uint32_t m_msgTimeoutCnt_ms;
bool m_newMessage = false;

// ----------------------------------------------------------------------------
// Write model-uydu telemetry packet respectively
struct TransmitingData{
  uint8_t temperature;
  float humidity;
  float AccX;
  float AccY;
  float AccZ;
  float GyroX;
  float GyroY;
  float GyroZ;
  uint8_t position;
  float distance;
}data;

// ----------------------------------------------------------------------------
ModelUydu modelUydu;
Adafruit_BNO055 bno;

// ----------------------------------------------------------------------------
Adafruit_BME280 bme; // I2C
//Adafruit_BME280 bme(BME_CS); // hardware SPI
//Adafruit_BME280 bme(BME_CS, BME_MOSI, BME_MISO, BME_SCK); // software SPI

// ----------------------------------------------------------------------------
void comHandler_reset()
{
  m_packetBufferInd = 0;
  m_newMessage = false;
}

// ----------------------------------------------------------------------------
void comHandler_init()
{
  uint8_t i = 0;
  for(i=0;i<MAX_LEN_INCOMING_DATA;i++)
  {
    m_packetBuffer[i] = 0;
  }
  comHandler_reset();
}

// ----------------------------------------------------------------------------
enum RequestCommand : uint8_t {
  UNKNOWN = 0,
  IMPELLENT,
  SEPARATION,
  DATATRANSFER,
  VIDEOINFO
};

// ----------------------------------------------------------------------------
void updateSensorData() {
   // ...
}

// ----------------------------------------------------------------------------
void sendSensorData() {
  // TODO transmiting data will set in here
  Serial.write(reinterpret_cast<char*>(&data),sizeof(data));
}

// ----------------------------------------------------------------------------
// Test example
void TestPrint() {
  Serial.println(F("----------------------------------------------------"));
  
  Serial.println(F("----------------------------------------------------"));
}

// ----------------------------------------------------------------------------
// Handle the command when command come from raspberrypi
void comHandle_handlerPacket()
{
  // RequestCommand requestCom = static_cast<RequestCommand>(m_packetBuffer[1]);
  switch((RequestCommand)m_packetBuffer[1])
  {
    // ------------------------------------------------------------------------
    // Tahrik komutu geldiği zaman işlenecek kod
    case IMPELLENT:
      break;

    // ------------------------------------------------------------------------
    // Ayrılma komutu geldiği zaman işlenecek kod
    case SEPARATION:
      break;

    // ------------------------------------------------------------------------
    // Veri isteme komutu geldiği zaman işlenecek kod
    case DATATRANSFER:
      #if(DEBUG)
          TestPrint();
      #else
          sendSensorData();
      #endif
      break;

    // ------------------------------------------------------------------------
    // Video aktarım bilgisi
    case VIDEOINFO:
      break;

    // ------------------------------------------------------------------------
    // Komut bilinmiyorsa işlenecek kod
    case UNKNOWN:
      break;
    
    // ------------------------------------------------------------------------
    default : break;
  }
}

// ----------------------------------------------------------------------------
// if arduino read any data from serial buffer, then software drop here.
// void serialEvent()
// {
//   while(Serial.available()) {

//     volatile uint8_t incomingData = Serial.read();

//     if(m_newMessage) {
//       //..
//     }

//     else {
//       m_packetBuffer[m_packetBufferInd] = incomingData;
//       m_packetBufferInd = m_packetBufferInd + 1;
//       if(incomingData > MAX_LEN_INCOMING_DATA) {
//         //Protect buffer overflow
//         m_newMessage = false;
//         m_packetBufferInd = 0;
//       }
//       else if(m_packetBufferInd == m_packetBuffer[0]) {
//         // Signal the main method for new message
//         m_packetBufferInd = 0;
//         m_newMessage = true;
//       }
//       else {
//         //If it takes longer than the frequency required for the pid algorithm in this loop,
//         //the pid algorithm will not work properly. Sellect correct frequency for pid then
//         // write pid frequency to m_msgTimeoutCnt_ms
//         m_msgTimeoutCnt_ms = 250;
//       }
//     }
//   }
// }


// ----------------------------------------------------------------------------
void timer_tick()
{
  // Packet timeout handler
  if(m_msgTimeoutCnt_ms > 0)
  {
    m_msgTimeoutCnt_ms -= 1;
    if(m_msgTimeoutCnt_ms == 0)
    {
      comHandler_reset();
    }
  }
}

// ----------------------------------------------------------------------------
void timer_init()
{
  Timer1.initialize(1 * 1000000);
  Timer1.attachInterrupt(timer_tick);
}

// ----------------------------------------------------------------------------
void uart_init()
{
  //usb.start(9600);
  Serial.begin(9600);
}


// ----------------------------------------------------------------------------
void setup() {
  uart_init();
  //comHandler_init();
  //timer_init();
  
  //myServo.attach(SERVO_PIN_OUT);
  //modelUydu.setSensor(&bno,GaziUzay::G_BNO055);
  //modelUydu.setSensor(&bme,GaziUzay::G_BME280);
  
  delay(2000);

  seperator.begin();
  relay.begin();
}

// ----------------------------------------------------------------------------
void loop() {
  // Update all sensor data 
  updateSensorData();
  
  // if any message come with serial port then send the all sensor data
  // if(m_newMessage) {
  //     comHandle_handlerPacket();
  //     m_newMessage = false;
  // }
  if(Serial.available()) {
        char c = Serial.read();
        Serial.println(c);
        if(c == '1') {
            Serial.println("On");
            seperator.run();
            relay.run();
        }
        else if(c=='2') {
            Serial.println("Off");
            seperator.close();
            relay.close();
        }
    }
}