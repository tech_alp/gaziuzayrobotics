#define _TASK_PRIORITY 

#include <Arduino.h> 

#include <TaskScheduler.h>
#include <DirectIO.h>
#include <Adafruit_BNO055.h>
#include <TinyGPS++.h>
#include "modeluydu2.hpp"
#include "gazisensor.hpp"
#include "motor.h"
#include "seperation.h"
#include "pin_util.h"
#include "config.h"
#include "RGBLed.h"
#include "Notify.h"

using namespace GU;

/* Set the delay between fresh samples */
uint16_t BNO055_SAMPLERATE_DELAY_MS = 100;
uint16_t BME280_SAMPLERATE_DELAY_MS = 100;
uint16_t GPS_ULTIMATE_SAMPLERATE_DELAY_MS = 1;

// ----------------------------------------------------------------------------
RGBLed statusLed(HAL::Pin::STATUS_RED_LED,HAL::Pin::STATUS_GREEN_LED,HAL::Pin::STATUS_BLUE_LED);
// Buzzer buzz(HAL::Pin::BUZZER);
Notify notify;

GaziSensor sensor;
ModelUydu2 modelUydu(&sensor);

Adafruit_BNO055 bno;
Adafruit_BME280 bme(BME_CS, BME_MOSI, BME_MISO, BME_SCK); // software SPI
TinyGPSPlus gps;

Scheduler ts,infoTS;

Motor& motor = Motor::get_singleton();

uint16_t pwm_val = 0;

///**************************************************
void updateBL();
Task tUpdate(50,-1,&updateBL,&infoTS,true);

void updateBL() {
    uint32_t time = millis();
    buzz.beeperUpdate(time);
    statusLed.update();
}

///**************************************************
void updateMC();
Task MCUpdate(100,-1,&updateMC,&ts,false);

void updateMC() {
    motor.output();
}

///**************************************************
void updateIMU();
Task IMUUpdate(BNO055_SAMPLERATE_DELAY_MS,-1,&updateIMU,&ts,false);

void updateIMU() {
    sensor.update_imu();
}

///**************************************************
void updateBME();
Task BMEUpdate(BME280_SAMPLERATE_DELAY_MS,-1,&updateBME,&ts,false);

void updateBME() {
    sensor.update_pressure();
    sensor.update_temperature();
}

///**************************************************
void updateGPS();
Task GPSUpdate(GPS_ULTIMATE_SAMPLERATE_DELAY_MS,-1,&updateGPS,&ts,false);

void updateGPS() {
    sensor.update_gps();
}

///**************************************************
void updatePS();
Task PSUpdate(1000,-1,&updatePS,&ts,false);

void updatePS() {
    Serial.println(F("------------------------------------"));
    Serial.print(F("Roll:       "));
    Serial.println(sensor.get_roll());
    Serial.print(F("Pitch:      "));
    Serial.println(sensor.get_pitch());
    Serial.print(F("Yaw:        "));
    Serial.println(sensor.get_yaw());
    Serial.print(F("Pressure:   "));
    Serial.println(sensor.get_pressure());
    Serial.print(F("Alt:        "));
    Serial.println(sensor.get_altitude());
    Serial.print(F("Vel:        "));
    Serial.println(sensor.get_velocity());
    Serial.print(F("Tmp:        "));
    Serial.println(sensor.get_temperature());
    Serial.print(F("Lng:        "));
    Serial.println(sensor.get_gps_longitude(),6);
    Serial.print(F("Lat:        "));
    Serial.println(sensor.get_gps_latitude(),6);
    Serial.print(F("Alt:        "));
    Serial.println(sensor.get_gps_altitude(),6);
    Serial.print(F("Date:       "));
    Serial.println(sensor.get_date());
    Serial.print(F("Time:       "));
    Serial.println(sensor.get_time());
    Serial.println(F("------------------------------------\n"));
}

///**************************************************
void serialEventCB();
Task tSerialEvent(50,-1,&serialEventCB,&infoTS,true);

void serialEventCB() {
    if(Serial.available()) {
        char ch = Serial.read();
        Serial.println(ch);
        if(ch == '0') { 
            motor.armed(true);
            if(motor.armed()) {
                // statusLed.color(Led::LedMode_e::ARMED_NO_GPS);
                buzz.beeper(Buzzer::beeperMode_e::BEEPER_ARMING_GPS_NO_FIX);
                Serial.println("armed");
            }
        }
        else if(ch == '1') {
            modelUydu.testMotor();
            if(motor.armed()) {
                buzz.beeper(Buzzer::beeperMode_e::BEEPER_MOTOR_TEST);
            }
            Serial.println(F("motor test started!"));
        }
        else if(ch == '2') {
            motor.motorStop();
            Serial.println(F("motor test stopped!"));
        }
        else if(ch == '3') {
            modelUydu.manuelSeperation();
            Serial.println(F("manual seperation activated!"));
        }
        else if(ch == '4') {
            modelUydu.getServoSeperation().close();
            Serial.println(F("manual seperation deactivated!"));
        }
        else if(ch == '5') {
            modelUydu.getSmokeSeperation().run();
            Serial.println(F("manual smoke activated!"));
        }
        else if(ch == '6') {
            modelUydu.getSmokeSeperation().close();
            Serial.println(F("manual smoke deactivated!"));
        }
        else if(ch == '7') {
            modelUydu.manualImpellent();
            Serial.println(F("manual impellent activated!"));
        }else if(ch == '8') {
            // statusLed.color(Led::LedMode_e::LOW_BATTERY);
            Serial.println(F("low battery!"));
        }
    }
}
///**************************************************



// ----------------------------------------------------------------------------
void uart_init() {
    Serial.begin(9600);
    GPS_UART.begin(9600);
}

// initialize pin
OutputPin outpin(static_cast<uint8_t>(HAL::Pin::POWER_LED));
void initPins(void) {
    outpin = HIGH;
    buzz.beeper(Buzzer::BEEPER_SYSTEM_INIT);
    // pinMode(HAL::Pin::POWER_LED,HAL::PinMode::OUT);
    // delay(5);
    // digitalWrite(HAL::Pin::POWER_LED,HAL::PinDigital::HI);
}

void setup() {
    uart_init();
    initPins();
    modelUydu.init();
    sensor.setSensor(&bno,GaziSensor::SensorType::G_BNO055);
    sensor.setSensor(&bme,GaziSensor::SensorType::G_BME280);
    sensor.setSensor(&gps,GaziSensor::SensorType::G_ADAFRUIT_ULTIMATE_GPS);
    PSUpdate.enableDelayed();
    IMUUpdate.enableDelayed();
    MCUpdate.enableDelayed();
    BMEUpdate.enableDelayed();
    GPSUpdate.enableDelayed();
    ts.setHighPriorityScheduler(&infoTS);
    delay(1000);
}

void loop() {
    ts.execute();
}