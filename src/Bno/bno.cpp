#include <Arduino.h>

#include "modeluydu.hpp"
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

ModelUydu modelUdyu;
Adafruit_BNO055 bno;

void updateBNO()
{
    modelUdyu.set_roll();
    modelUdyu.set_pitch();
    modelUdyu.set_yaw();
}

void setup()
{
    Serial.begin(9600);
    modelUdyu.setSensor(&bno,GaziUzay::G_BNO055);
    modelUdyu.PrintSensorDetails(modelUdyu.getG_BNO());
}

void loop()
{
    updateBNO();
    Serial.print(F("Rool:     "));
    Serial.println(modelUdyu.Roll());
    Serial.print(F("Pitch:    "));
    Serial.println(modelUdyu.Pitch());
    Serial.print(F("Yaw:      "));
    Serial.println(modelUdyu.Yaw());
    Serial.println(modelUdyu.TelemetryPackage());
    delay(1000);
}