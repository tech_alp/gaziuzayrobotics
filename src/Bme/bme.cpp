#include <Arduino.h>

#include "config.h"
#include "modeluydu.hpp"
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

ModelUydu modelUydu;
Adafruit_BME280 bme(10,11,12,13);

void updateBME()
{
    modelUydu.set_temperature(); 
    modelUydu.set_pressure();
    modelUydu.set_altitude();
    modelUydu.set_velocity();
}

void setup()
{
    Serial.begin(9600);
    modelUydu.setSensor(&bme,GaziUzay::G_BME280);
    modelUydu.PrintSensorDetails(modelUydu.getG_BNO());
    // Function is calibration of altitude.
    modelUydu.altitudeCalibration();
}

void loop()
{
    updateBME();
    Serial.println(F("------------------------------------"));
    Serial.print(F("Temperature:    "));
    Serial.print(modelUydu.Temperature());
    Serial.println(" °C");
    Serial.print(F("Pressure:       "));
    Serial.print(modelUydu.Pressure());
    Serial.println(" hPa");
    Serial.print(F("Altitude:       "));
    Serial.print(modelUydu.Altitude());
    Serial.println(" m");
    Serial.print(F("Velocity:       "));
    Serial.print(modelUydu.Velocity());
    Serial.println(" m/s");
    Serial.println(F("------------------------------------\n"));

    //Serial.println(modelUydu.TelemetryPackage());
    delay(1000);
}
