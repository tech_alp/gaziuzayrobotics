#ifndef _GaziSensor_H
#define _GaziSensor_H

// class GU_Baro;
// class GU_InertialSensor;

#include <Adafruit_BME280.h>
#include <Adafruit_BNO055.h>
#include <TinyGPS++.h>
#include "Filter.h"
#include "config.h"


class GaziSensor
{
friend class ModelUydu2;
public:
    GaziSensor();
    
    static GaziSensor* get_singleton() {
        return m_singleton;
    }

    enum SensorType {
        G_NONE,
        G_BNO055,
        G_BME280,
        G_ADAFRUIT_ULTIMATE_GPS
    };

    enum SensorState {
        NotFound = 0,
        Connecting,
        Disconnecting,
        Error
    };

    struct Gazi_Sensor_t
    {
        char name[12];
        int32_t type;
        int32_t sensorState;
        int32_t timestamp;
    };

    GaziSensor& setSensor(void* sensor,GaziSensor::SensorType sensorName);
    
    void initBME();
    void initBNO();
    void initGPS();

    void altitude_calibration();

    void update_imu();
    void update_pressure();
    void update_temperature();
    void update_gps();

	float get_pressure() const;
	float get_altitude() const;
	float get_velocity() const;
	float get_temperature() const;
	float get_gps_latitude() const;
	float get_gps_longitude() const;
	float get_gps_altitude() const;
	int get_pitch() const;
	int get_roll() const;
	int get_yaw() const;
    String get_date() const;
    String get_time() const;

    struct Sensors_s {
        float m_avg_pressure = 0.f;
        float m_avg_velocity_z = 0.f;
        float m_avg_altitude = 0.f;
    };
    Sensors_s sensor;

private:
    static GaziSensor* m_singleton;
    Adafruit_BME280* m_bme{nullptr};
    Adafruit_BNO055* m_bno{nullptr};
    TinyGPSPlus*  m_gps{nullptr};
    Gazi_Sensor_t g_bno{"BNO055",G_BNO055,NotFound,0};
    Gazi_Sensor_t g_bme{"BME280",G_BME280,NotFound,0};
    Gazi_Sensor_t g_gps{"ULTIMATEGPS",G_ADAFRUIT_ULTIMATE_GPS,NotFound,0};

    // Variables defined for BME280
	float m_pressure{};
	float m_temperature{};
    float m_altitude;
    float m_velocity;
	
    // Variables defined for BNO055
	int m_pitch{};
	int m_roll{};
	int m_yaw{};

    // Variables defined for GPS
	float m_gps_latitude{};
	float m_gps_longitude{};
	float m_gps_altitude{};
    String m_date;
    String m_time;

    float m_baselinePressure{SEALEVELPRESSURE_HPA};

    AverageFilter<float,float,10> m_pressure_filter;
    // AverageFilterFloat_Size5 m_pressure_filter;
    //float calculate_velocity_z();
};

#endif