#include "GaziSensor.hpp"
#include <GU_Baro.h>
#include <GU_InertialSensor.h>
#include "Notify.h"

namespace {
    uint32_t prev_time = 0;
    float    prev_alt  = 0;
    float calculate_altitude(float pressure,float baselinePressure) {
        float avg_alt = (44330.0*(1-pow(pressure/baselinePressure,0.1903)));
        if(avg_alt < 0)	// If altitude is measured below zero due to margin of error, the altitude is considered zero.
        {
            avg_alt = 0;
        }
        return avg_alt;
    }
    float  calculate_velocity_z(float current_alt)
    {

        // Look pitot tube working mechanism
        unsigned long currentTime = millis(); // This millis function defined for set_velocity() function.

        float timeDifference		= static_cast<float>(currentTime - prev_time)/1000;
        float altitudeDifference	= current_alt - prev_alt;
        
        // Altitude difference divided by time difference gives us the velocity value.
        float velocity = altitudeDifference / timeDifference;
        
        // Identification processes.				
        prev_time = currentTime;	// Define for the previous time variable value.
        prev_alt = current_alt;	    // Define for the previous altitude variable value.
        return velocity;
    }
    void update_date_time(TinyGPSPlus* gps,String& date,String& time)
    {
        TinyGPSDate &d = gps->date;
        TinyGPSTime &t = gps->time;
        if (d.isValid()) {
            char sz[32];
            sprintf(sz, "%02d/%02d/%02d ", d.day(),d.month(), d.year());
            date = sz;
        } 
        if (t.isValid()) {
            char sz[32];
            sprintf(sz, "%02d:%02d:%02d ", t.hour() > 22 ? t.hour()%22+1 : t.hour()+3, t.minute(), t.second());
            time = sz;
        }
    }

}

GaziSensor* GaziSensor::m_singleton = nullptr;

GaziSensor::GaziSensor()
{
    if(m_singleton) {
        //Fail gazisensor singleton class
        DEBUG_PRINT("GaziSensor must be singleton");
    }
    m_singleton = this;
}

// ----------------------------------------------------------------------------
GaziSensor& GaziSensor::setSensor(void* sensor,GaziSensor::SensorType sensorName)
{
    switch(sensorName)
    {
        // ------------------------------------------------------------------------
        case SensorType::G_BNO055: {
            //g_bno.bno = reinterpret_cast<Adafruit_BNO055*>(sensor);
            m_bno = reinterpret_cast<Adafruit_BNO055*>(sensor);
            initBNO();
            break;
        }
        
        // ------------------------------------------------------------------------
        case SensorType::G_BME280: {
            // g_bno.bme = reinterpret_cast<Adafruit_BME280*>(sensor);
            m_bme = reinterpret_cast<Adafruit_BME280*>(sensor);
            initBME();
            break;
        }

        case SensorType::G_ADAFRUIT_ULTIMATE_GPS: {
            m_gps = static_cast<TinyGPSPlus*>(sensor);
            initGPS();
            break;
        }

        // ------------------------------------------------------------------------
        // TODO exeption handling on arduino serial port!
        default:
            Serial.println(sensorName + " Not Found!");
    }
    return *this;
}

// ----------------------------------------------------------------------------
//Setup function for the BME280 sensor.
void GaziSensor::initBME()
{
    if (!m_bme->begin())
	{
        #if(DEBUG)
            Serial.println("The BME280 sensor could not be initialized. Check your wiring or I2C ADDR!");
        #endif
        g_bme.sensorState = Error;
        Notify::flags.gps_glitching = true;
    }
    else
    {
        g_bme.sensorState = Connecting;
    }
}

// ----------------------------------------------------------------------------
//Initiate function for the BNO055 sensor.
void GaziSensor::initBNO()
{
	if(!m_bno->begin()) 
    {
        #if(DEBUG)
            Serial.println("The BNO055 sensor could not be initialized. Check your wiring or I2C ADDR!");
        #endif
        g_bno.sensorState = Error;
        Notify::flags.error = true;
	}
    else
    {
	    // Use external crystal for better accuracy.
	    m_bno->setExtCrystalUse(true);
        g_bno.sensorState = Connecting;
    }
}

// ----------------------------------------------------------------------------
//// Initialize function for alternative gps sensor GPS NEO.
void GaziSensor::initGPS()
{
    //TODO: control motor is armed or not.
    // _status_led->color(Led::LedMode_e::READY_GPS);
    // _buzz->beeper(Buzzer::beeperMode_e::BEEPER_ARMING_GPS_NO_FIX);
// 	gps.startSerial(_baud);
// 	delay(1000);
// 	gps.setSentencesToReceive(OUTPUT_GGA); // We are using GGA protocol.
}

// ----------------------------------------------------------------------------
//// Calibration function for initial settings.
void GaziSensor::altitude_calibration()
{
    m_baselinePressure = m_bme->readPressure();
}

void GaziSensor::update_imu()
{
    imu::Quaternion quat = m_bno->getQuat();
    double yy = quat.y() * quat.y();

    double pitchRad = asin (2 *  quat.w() * quat.y() - quat.x() * quat.z());
    double rollRad  = atan2(2 * (quat.w() * quat.x() + quat.y() * quat.z()),1 - 2*(quat.x() * quat.x() + yy));
    double yawRad   = atan2(2 * (quat.w() * quat.z() + quat.x() * quat.y()), 1 - 2*(yy+quat.z() * quat.z()));

    m_pitch = 57.2958 * pitchRad;
    m_roll  = 57.2958 * rollRad;
    m_yaw   = 57.2958 * yawRad;
}

void GaziSensor::update_pressure()
{
    m_pressure = m_bme->readPressure() / 100.0F;
    sensor.m_avg_pressure = m_pressure_filter.apply(m_pressure);
    sensor.m_avg_altitude = calculate_altitude(sensor.m_avg_pressure,SEALEVELPRESSURE_HPA);
    m_altitude = m_bme->readAltitude(SEALEVELPRESSURE_HPA);
    sensor.m_avg_velocity_z = calculate_velocity_z(sensor.m_avg_altitude);
}

void GaziSensor::update_temperature()
{
    m_temperature = m_bme->readTemperature();
}

static uint32_t prevTimer = 0;
void GaziSensor::update_gps()
{
    static uint32_t timer = millis();
    if(GPS_UART.available()) {
        if(m_gps->encode(GPS_UART.read())) {
            if(timer - prevTimer > 1000) {
                if(m_gps->location.isValid()) {
                    m_gps_latitude = m_gps->location.lat();
                    m_gps_longitude = m_gps->location.lng();
                    m_gps_altitude = m_gps->altitude.meters();
                }
                update_date_time(m_gps,m_date,m_time);
                Notify::flags.gps_num_sats = static_cast<uint8_t>(m_gps->satellites.value());
            }
        }
    }
}

float GaziSensor::get_pressure() const
{
    return m_pressure;
}

float GaziSensor::get_altitude() const
{
    return m_altitude;
}

float GaziSensor::get_velocity() const
{
    return sensor.m_avg_velocity_z;
}

float GaziSensor::get_temperature() const
{
    return m_temperature;
}

float GaziSensor::get_gps_latitude() const
{
    return m_gps_latitude;
}

float GaziSensor::get_gps_longitude() const
{
    return m_gps_longitude;
}

float GaziSensor::get_gps_altitude() const
{
    return m_gps_altitude;
}

int GaziSensor::get_pitch() const
{
    return m_pitch;
}

int GaziSensor::get_roll() const
{
    return m_roll;
}

int GaziSensor::get_yaw() const
{
    return m_yaw;
}

String GaziSensor::get_date() const
{
    return m_date;
}

String GaziSensor::get_time() const
{
    return m_time;
}