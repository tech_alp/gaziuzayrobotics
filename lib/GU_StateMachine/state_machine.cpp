#include "state_machine.h"
#include "gazisensor.hpp"

StateMachine* StateMachine::_singleton;

GaziSensor* gSensor = GaziSensor::get_singleton();

static uint32_t prev_altitude = 0;
static bool rising()
{
    if(gSensor->sensor.m_avg_altitude > prev_altitude) {
        prev_altitude = gSensor->sensor.m_avg_altitude;
        return true;
    }
    return false;
}

StateMachine::StateMachine(StateId_e initial_state)
    : _current_id(initial_state), _motion(MotionState_e::STEADYSTATE),
    _ascent_state(true),_launchpad_state(true)
{
    if(_singleton) {
        //There is a existing motor instance
        //TODO return error
    }
    _singleton = this;
}

void StateMachine::updateState()
{
    float alt = gSensor->sensor.m_avg_altitude;
    if(alt < 20) {
        if(_launchpad_state) {
            //0
            //LaunchPad
            _current_id = StateId_e::LAUNCHPAD;
        }
        else {
            //9
            //Landing
            _current_id = StateId_e::LANDING;
        }
    }
    else if(alt > 20 && _ascent_state) {
        //1
        //Ascent
        _current_id = StateId_e::ASCENT; 
    }
    else if(alt > 400 && _ascent_state == false) {

    }
}

void StateMachine::updateMotion()
{
    if(rising())
        _motion = MotionState_e::RISINGSTATE;
    else
        _motion = MotionState_e::FALLINGSTATE;
}