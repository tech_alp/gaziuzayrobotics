#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include "status_enum.h"

class StateMachine {
public:
    StateMachine(StateId_e initial_state = StateId_e::LAUNCHPAD);

    /*  Do not allow copy  */
    StateMachine(const StateMachine&) = delete;
    StateMachine& operator=(const StateMachine&) = delete;

    static StateMachine& getSingleton() {
        return *_singleton;
    }

    void setState(StateId_e state) { _current_id = state; }

    StateId_e getState()const { return _current_id; }

    MotionState_e getMotionState()const { return _motion; }
    
    void updateState();
    void updateMotion();

private:
    static StateMachine* _singleton;
    StateId_e _current_id;
    MotionState_e _motion;
    bool _ascent_state;
    bool _launchpad_state;
};

#endif