#include "RgbLed.h"
#include <Arduino.h>
#include "Notify.h"

RGBLed::RGBLed(uint8_t r,uint8_t g,uint8_t b) 
    : _red_led(r), _green_led(g), _blue_led(b)
{
    pinMode(_red_led,OUTPUT);
    pinMode(_green_led,OUTPUT);
    pinMode(_blue_led,OUTPUT);

    analogWrite(_red_led,LED_OFF);
    analogWrite(_green_led,LED_OFF);
    analogWrite(_blue_led,LED_OFF);
}

uint32_t RGBLed::get_colour_sequence() const
{
    // initialising pattern
    if (Notify::flags.initialising) {
        return sequence_initialising;
    }

    if(Notify::flags.error) {
        return sequence_error;
    }

    if (Notify::flags.esc_calibration) {
        return sequence_trim_or_esc;
    }

    // radio and battery failsafe patter: flash yellow
    // gps failsafe pattern : flashing yellow and blue
    if (Notify::flags.failsafe_battery ||
        Notify::flags.gps_glitching) {
            if (Notify::flags.gps_glitching) {
                // blue on gps glitch
                return sequence_failsafe_gps_glitching;
            }
        // all off for radio or battery failsafe
        return sequence_failsafe_battery;
    }

    else if(Notify::flags.armed) {
        // solid green if armed with GPS least 3 nums
        if (Notify::flags.gps_num_sats >= 3) {
            return sequence_armed;
        }
        // solid blue if armed with no GPS
        return sequence_armed_nogps;
    }

    return sequence_disarmed_bad_gps;
}

bool RGBLed::init()
{
    return true;
}

void RGBLed::turn_on() {
    analogWrite(_red_led,_red_curr);
    analogWrite(_green_led,_green_curr);
    analogWrite(_blue_led,_blue_curr);
}

void RGBLed::update()
{
    uint32_t current_colour_sequence = get_colour_sequence();

    const uint8_t brightness = LED_BRIGHT;

    uint8_t step = (millis()/100) % 10;

    // ensure we can't skip a step even with awful timing
    if (step != last_step) {
        step = (last_step+1) % 10;
        last_step = step;
    }

    const uint8_t colour = (current_colour_sequence >> (step*3)) & 7;

    uint8_t red_des = (colour & RED) ? brightness : LED_OFF;
    uint8_t green_des = (colour & GREEN) ? brightness : LED_OFF;
    uint8_t blue_des = (colour & BLUE) ? brightness : LED_OFF;
    // Serial.print(red_des);
    // Serial.print(" ");
    // Serial.print(green_des);
    // Serial.print(" ");
    // Serial.println(blue_des);
    set_rgb(red_des, green_des, blue_des);
    turn_on();
}

void RGBLed::set_rgb(uint8_t red, uint8_t green, uint8_t blue)
{
    _red_curr = red;
    _green_curr = green;
    _blue_curr = blue;
}
