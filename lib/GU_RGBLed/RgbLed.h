#ifndef _RGBLED_H
#define _RGBLED_H

#include <stdint.h>
#include "NotifyDevice.h"

#define LED_OFF    0x0
#define LED_BRIGHT 0xFF
#define LED_MEDIUM 0XAA
#define LED_DIM    0x55

class RGBLed : public NotifyDevice {
public:
    RGBLed(uint8_t r,uint8_t g,uint8_t b);

    virtual bool init() override;

    // update - updates led according to timed_updated. Should be
    // called at 50Hz
    virtual void update() override;

    // set_rgb - set color as a combination of red, green and blue levels from 0 ~ 15
    void set_rgb(uint8_t red, uint8_t green, uint8_t blue);

    void turn_on();
private:
    uint8_t _red_curr, _green_curr, _blue_curr;  // current colours displayed by the led
    uint8_t _red_led;
    uint8_t _green_led;
    uint8_t _blue_led;

    uint32_t get_colour_sequence() const;
    
#define DEFINE_COLOUR_SEQUENCE(S0, S1, S2, S3, S4, S5, S6, S7, S8, S9)  \
    ((S0) << (0*3) | (S1) << (1*3) | (S2) << (2*3) | (S3) << (3*3) | (S4) << (4*3) | (S5) << (5*3) | (S6) << (6*3) | (S7) << (7*3) | (S8) << (8*3) | (S9) << (9*3))

#define DEFINE_COLOUR_SEQUENCE_SLOW(colour) \
    DEFINE_COLOUR_SEQUENCE(colour,colour,colour,colour,colour,BLACK,BLACK,BLACK,BLACK,BLACK)
#define DEFINE_COLOUR_SEQUENCE_FAILSAFE(colour) \
    DEFINE_COLOUR_SEQUENCE(YELLOW,YELLOW,YELLOW,YELLOW,YELLOW,colour,colour,colour,colour,colour)
#define DEFINE_COLOUR_SEQUENCE_SOLID(colour) \
    DEFINE_COLOUR_SEQUENCE(colour,colour,colour,colour,colour,colour,colour,colour,colour,colour)
#define DEFINE_COLOUR_SEQUENCE_ALTERNATE(colour1, colour2) \
    DEFINE_COLOUR_SEQUENCE(colour1,colour2,colour1,colour2,colour1,colour2,colour1,colour2,colour1,colour2)

#define BLACK  ((uint32_t)0)
#define BLUE   ((uint32_t)1)
#define GREEN  ((uint32_t)2)
#define RED    ((uint32_t)4)
#define YELLOW ((uint32_t)(RED|GREEN))
#define WHITE  ((uint32_t)(RED|GREEN|BLUE))

    const uint32_t sequence_initialising = DEFINE_COLOUR_SEQUENCE_ALTERNATE(RED,BLUE);
    const uint32_t sequence_trim_or_esc = DEFINE_COLOUR_SEQUENCE(RED,BLUE,GREEN,RED,BLUE,GREEN,RED,BLUE,GREEN,BLACK);
    const uint32_t sequence_failsafe_gps_glitching = DEFINE_COLOUR_SEQUENCE_FAILSAFE(BLUE);
    const uint32_t sequence_failsafe_battery = DEFINE_COLOUR_SEQUENCE_FAILSAFE(BLACK);

    const uint32_t sequence_armed = DEFINE_COLOUR_SEQUENCE_SOLID(GREEN);
    const uint32_t sequence_armed_nogps = DEFINE_COLOUR_SEQUENCE_SOLID(BLUE);
    const uint32_t sequence_prearm_failing = DEFINE_COLOUR_SEQUENCE(YELLOW,YELLOW,BLACK,BLACK,YELLOW,YELLOW,BLACK,BLACK,BLACK,BLACK);
    const uint32_t sequence_disarmed_good_dgps = DEFINE_COLOUR_SEQUENCE_ALTERNATE(GREEN,BLACK);
    const uint32_t sequence_error = DEFINE_COLOUR_SEQUENCE(RED,BLACK,RED,BLACK,RED,BLACK,RED,BLACK,RED,BLACK);
    const uint32_t sequence_disarmed_good_gps = DEFINE_COLOUR_SEQUENCE_SLOW(GREEN);
    const uint32_t sequence_disarmed_bad_gps = DEFINE_COLOUR_SEQUENCE_SLOW(BLUE);

    uint8_t last_step;
};

#endif