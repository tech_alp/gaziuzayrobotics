#ifndef _UTIL_HPP
#define _UTIL_HPP

// saturation function
template<typename T>
inline T satu(T num, T maxi, T mini) {
    if(num>=maxi)
        num=maxi;
    else if(num <= mini)
        num=mini;
    return num;
}

#endif