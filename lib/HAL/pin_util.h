#ifndef PIN_UTILS_H
#define PIN_UTILS_H

#include <stdint.h>
#include "gpio.h"
#include <Arduino.h>

namespace HAL {
    
enum class PinMode { IN = 0, OUT = 1 };

// TODO HIGH and LOW conflict with existing macro. We should adopt a better
// naming convention for enums.
enum class PinDigital { LO = 0, HI = 1 };

inline void pinMode(HAL::Pin pin, HAL::PinMode mode) {
    ::pinMode(static_cast<uint8_t>(pin), static_cast<uint8_t>(mode));
}

inline void digitalWrite(HAL::Pin pin, HAL::PinDigital val) {
    ::digitalWrite(static_cast<uint8_t>(pin), static_cast<uint8_t>(val));
}

inline int analogRead(HAL::Pin pin) { return ::analogRead(static_cast<uint8_t>(pin)); }

inline void analogWrite(HAL::Pin pin, int val) { ::analogWrite(static_cast<uint8_t>(pin),val); }

// int LED_BUILTIN();
} // namespace Hal

#endif