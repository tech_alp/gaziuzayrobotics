#ifndef _GPIO_H
#define _GPIO_H
/*
 * Pin 13 -> built-in LED (labeled LED3) The LED will be on if all sensors have been
 * successfully initialized The LED will blink if there is a non-critical failure
 * The LED will not turn on if there is a critical failure
 *
 * Pin 20 -> Power status LED (labeled LED2) The LED will turn on if there is power to the microcontroller
 * 
 * Pin 21 -> Flight status LED (labeled LED1) The LED will turn on in ARMED
*/
namespace HAL {

enum Pin : uint8_t {
    FLIGHT_LED            = 21, // required by state machine
    BUILTIN_LED           = 13,
    POWER_LED             = 39, // uses BUILTIN_LED
    BUZZER                = 30, // output pin
    SEPERATION_SERVO_PIN  = 23, // pwm output pin
    MOTOR_PIN             = 14, // pwm output pin
    FIRE_SMOKE_PIN        = 28, // digital output pin
    BATTERY_PIN           = 33, // analog input pin
    STATUS_BLUE_LED       = 6,  //35,
    STATUS_GREEN_LED      = 5,  //36,
    STATUS_RED_LED        = 3,  //37,
    SEPERATION_CAMERA_PIN = 7
};

}
#endif