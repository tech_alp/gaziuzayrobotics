
#ifndef CONFIG_HPP
#define CONFIG_HPP

/////////////////////////////////////////////////////
//  APP_VERSION     "1.0.0"
//  APP_DEVELOPER   "Enes Alp"
//  APP_NAME        "GAZI UZAY"
//  APP_SUPPORT_URL ""
//
// ***************************************************
// * This software supports following sensors
// * - BNO055
// * - BME280
// ***************************************************
//
/////////////////////////////////////////////////////

/**
 * if you want to see data with human readable format, use true
 * False is transmiting data with uint8_t type and if you open 
 * on the serial port, you see non human readable format
 */
//#define DEBUG false //true

#include <Arduino.h>

#ifdef DEBUG
    #define DEBUG_PRINT(x) Serial.println(x)
#else
    #define DEBUG_PRINT(x)
#endif

// ----------------------------------------------------------------------------
#define MAX_LEN_TRANSMITING_DATA    100
#define MAX_LEN_INCOMING_DATA       100

// ----------------------------------------------------------------------------
// Defines pin number to which the sensor is connected
#define SERVO_PIN_OUT 9


// ----------------------------------------------------------------------------
// Defines BME sensor pins
#define BME_SCK 32
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10
#define SEALEVELPRESSURE_HPA (1013.25)

// ----------------------------------------------------------------------------
// Defines BNO sensor pins


// ----------------------------------------------------------------------------
// Defines GPS sensor pins
#define GPS_UART_RX     0    // RX1
#define GPS_UART_TX     1    // TX1


#define GPS_UART        Serial1

#endif