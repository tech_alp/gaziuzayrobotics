#ifndef FILTERCLASS_H
#define FILTERCLASS_H

#include <stdint.h>

template<typename T>
class Filter {
public:
    virtual ~Filter() {}

    // apply - Add a new raw value to filter, retrieve the filtered result
    virtual T apply(T sample) = 0;

    // reset - Clear the filter
    virtual void reset() = 0;
};

// Typedef for convenience
typedef Filter<int8_t> FilterInt8;
typedef Filter<uint8_t> FilterUInt8;
typedef Filter<int16_t> FilterInt16;
typedef Filter<uint16_t> FilterUInt16;
typedef Filter<int32_t> FilterInt32;
typedef Filter<uint32_t> FilterUInt32;

#endif