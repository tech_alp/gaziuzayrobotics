#ifndef FILTERWITHBUFFER_H
#define FILTERWITHBUFFER_H

#include "FilterClass.h"

template<typename T, uint8_t FILTER_SIZE>
class FilterWithBuffer : public Filter<T> {
public:
    // constructor
    FilterWithBuffer();

    // destructor
    virtual ~FilterWithBuffer() {}

    // apply - Add a new raw value to the filter, retrieve the filtered result
    T apply(T sample)override;

    // reset - Clear the filter
    void reset()override;

    // get filter size
    uint8_t getFilterSize()const {
        return FILTER_SIZE;
    }

    T get_sample(uint8_t i)const {
        return samples[i];
    }

protected:
    T samples[FILTER_SIZE];    // buffer of samples
    uint8_t sample_index;      // pointer to the next empty slot in the buffer
};

// Typedef for convenience
typedef FilterWithBuffer<int16_t,2> FilterWithBufferInt16_Size2;
typedef FilterWithBuffer<int16_t,3> FilterWithBufferInt16_Size3;
typedef FilterWithBuffer<int16_t,4> FilterWithBufferInt16_Size4;
typedef FilterWithBuffer<int16_t,5> FilterWithBufferInt16_Size5;
typedef FilterWithBuffer<int16_t,6> FilterWithBufferInt16_Size6;
typedef FilterWithBuffer<int16_t,7> FilterWithBufferInt16_Size7;
typedef FilterWithBuffer<uint16_t,2> FilterWithBufferUInt16_Size2;
typedef FilterWithBuffer<uint16_t,3> FilterWithBufferUInt16_Size3;
typedef FilterWithBuffer<uint16_t,4> FilterWithBufferUInt16_Size4;
typedef FilterWithBuffer<uint16_t,5> FilterWithBufferUInt16_Size5;
typedef FilterWithBuffer<uint16_t,6> FilterWithBufferUInt16_Size6;
typedef FilterWithBuffer<uint16_t,7> FilterWithBufferUInt16_Size7;

typedef FilterWithBuffer<int32_t,2> FilterWithBufferInt32_Size2;
typedef FilterWithBuffer<int32_t,3> FilterWithBufferInt32_Size3;
typedef FilterWithBuffer<int32_t,4> FilterWithBufferInt32_Size4;
typedef FilterWithBuffer<int32_t,5> FilterWithBufferInt32_Size5;
typedef FilterWithBuffer<int32_t,6> FilterWithBufferInt32_Size6;
typedef FilterWithBuffer<int32_t,7> FilterWithBufferInt32_Size7;
typedef FilterWithBuffer<uint32_t,2> FilterWithBufferUInt32_Size2;
typedef FilterWithBuffer<uint32_t,3> FilterWithBufferUInt32_Size3;
typedef FilterWithBuffer<uint32_t,4> FilterWithBufferUInt32_Size4;
typedef FilterWithBuffer<uint32_t,5> FilterWithBufferUInt32_Size5;
typedef FilterWithBuffer<uint32_t,6> FilterWithBufferUInt32_Size6;
typedef FilterWithBuffer<uint32_t,7> FilterWithBufferUInt32_Size7;

// Constructor
template<typename T, uint8_t FILTER_SIZE>
FilterWithBuffer<T,FILTER_SIZE>::FilterWithBuffer() : sample_index(0)
{
    // clear sample buffer
    reset();
}

template<typename T, uint8_t FILTER_SIZE>
T FilterWithBuffer<T,FILTER_SIZE>::apply(T sample)
{
    samples[sample_index++] = sample;
    
    if(sample_index >= FILTER_SIZE)
        sample_index = 0;
    
    // base class doesn't know what filtering to do so we just return the raw sample
    return sample;
}

template<typename T, uint8_t FILTER_SIZE>
void FilterWithBuffer<T,FILTER_SIZE>::reset()
{
    // clear samples buffer
    for(uint8_t i = 0; i < FILTER_SIZE; ++i) {
        samples[i] = 0;
    }

    // reset index back to beginning of the array
    sample_index = 0;
}

#endif