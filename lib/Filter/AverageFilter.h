#ifndef AVERAGEFILTER_H
#define AVERAGEFILTER_H

#include "FilterWithBuffer.h"

// 1st parameter <T> is the type of data being filtered.
// 2nd parameter <U> is a larger data type used during summation to prevent overflows
// 3rd parameter <FILTER_SIZE> is the number of elements in the filter
template<typename T, class U, uint8_t FILTER_SIZE>
class AverageFilter : public FilterWithBuffer<T,FILTER_SIZE> {
public:
    AverageFilter() : FilterWithBuffer<T,FILTER_SIZE>(), _num_samples(0) {}

    T apply(T sample)override;

    void reset()override;

protected:
    // the number of samples in the filter, maxes out at size of the filter
    uint8_t        _num_samples;
};

// Typedef for convenience (1st argument is the data type, 2nd is a larger datatype to handle overflows, 3rd is buffer size)
typedef AverageFilter<int8_t,int16_t,2> AverageFilterInt8_Size2;
typedef AverageFilter<int8_t,int16_t,3> AverageFilterInt8_Size3;
typedef AverageFilter<int8_t,int16_t,4> AverageFilterInt8_Size4;
typedef AverageFilter<int8_t,int16_t,5> AverageFilterInt8_Size5;
typedef AverageFilter<uint8_t,uint16_t,2> AverageFilterUInt8_Size2;
typedef AverageFilter<uint8_t,uint16_t,3> AverageFilterUInt8_Size3;
typedef AverageFilter<uint8_t,uint16_t,4> AverageFilterUInt8_Size4;
typedef AverageFilter<uint8_t,uint16_t,5> AverageFilterUInt8_Size5;

typedef AverageFilter<int16_t,int32_t,2> AverageFilterInt16_Size2;
typedef AverageFilter<int16_t,int32_t,3> AverageFilterInt16_Size3;
typedef AverageFilter<int16_t,int32_t,4> AverageFilterInt16_Size4;
typedef AverageFilter<int16_t,int32_t,5> AverageFilterInt16_Size5;
typedef AverageFilter<uint16_t,uint32_t,2> AverageFilterUInt16_Size2;
typedef AverageFilter<uint16_t,uint32_t,3> AverageFilterUInt16_Size3;
typedef AverageFilter<uint16_t,uint32_t,4> AverageFilterUInt16_Size4;
typedef AverageFilter<uint16_t,uint32_t,5> AverageFilterUInt16_Size5;
 
typedef AverageFilter<int32_t,float,2> AverageFilterInt32_Size2;
typedef AverageFilter<int32_t,float,3> AverageFilterInt32_Size3;
typedef AverageFilter<int32_t,float,4> AverageFilterInt32_Size4;
typedef AverageFilter<int32_t,float,5> AverageFilterInt32_Size5;
typedef AverageFilter<uint32_t,float,2> AverageFilterUInt32_Size2;
typedef AverageFilter<uint32_t,float,3> AverageFilterUInt32_Size3;
typedef AverageFilter<uint32_t,float,4> AverageFilterUInt32_Size4;
typedef AverageFilter<uint32_t,float,5> AverageFilterUInt32_Size5;

typedef AverageFilter<float,float,5> AverageFilterFloat_Size5;

// Public Methods //////////////////////////////////////////////////////////////
template<typename T, class U, uint8_t FILTER_SIZE>
T AverageFilter<T,U,FILTER_SIZE>::apply(T sample)
{
    U result = 0;

    // call parent's apply function to get the sample into the array
    FilterWithBuffer<T,FILTER_SIZE>::apply(sample);

    // increment the number of samples so far
    ++_num_samples;
    if(_num_samples > FILTER_SIZE || _num_samples == 0)
        _num_samples = FILTER_SIZE;

    // get sum of all values - there is a risk of overflow here that we ignore
    for(uint8_t i = 0; i < FILTER_SIZE; ++i) {
        result += FilterWithBuffer<T,FILTER_SIZE>::samples[i];
    }

    return static_cast<T>(result/_num_samples);
}

template<typename T, class U, uint8_t FILTER_SIZE>
void AverageFilter<T,U,FILTER_SIZE>::reset()
{
    // call parent's apply function to get the sample into the array
    FilterWithBuffer<T,FILTER_SIZE>::reset();

    // clear our variable
    _num_samples = 0;
}


/*
 * This filter is intended to be used with integral types to be faster and
 * avoid loss of precision on floating point arithmetic. The integral type
 * chosen must be one that fits FILTER_SIZE values you are filtering.
 *
 * Differently from other average filters, the result is only returned when
 * getf()/getd() is called
 */
template<typename T, class U, uint8_t FILTER_SIZE>
class AverageIntegralFilter : public AverageFilter<T,U,FILTER_SIZE> {
public:

    T apply(T sample)override;

    void reset()override;

    float getf()const;

    double getd()const;

protected:
    U _sum = 0;
};

template<typename T, class U, uint8_t FILTER_SIZE>
T AverageIntegralFilter<T,U,FILTER_SIZE>::apply(T sample)
{
    T curr = this->samples[this->sample_index];

    FilterWithBuffer<T,FILTER_SIZE>::apply(sample);

    ++this->_num_samples;
    if(this->_num_samples > FILTER_SIZE || this->_num_samples == 0)
        this->_num_samples = FILTER_SIZE;
    
    _sum -= curr;
    _sum += sample;

    // don't return the value: caller is forced to call getf() or getd()
    return 0;
}

template<typename T, class U, uint8_t FILTER_SIZE>
float AverageIntegralFilter<T,U,FILTER_SIZE>::getf()const
{
    if(_sum = 0)
        return 0.f;
    
    return static_cast<float>(_sum)/this->_num_samples;
}

template<typename T, class U, uint8_t FILTER_SIZE>
double AverageIntegralFilter<T,U,FILTER_SIZE>::getd()const
{
    if(_sum = 0)
        return 0.;
    
    return static_cast<double>(_sum)/this->_num_samples;
}


#endif