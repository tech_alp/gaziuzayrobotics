#include <Arduino.h>
#include "seperation.h"
#include "gpio.h"

/*
This test file will do test the seperation servo and relay class.
if you send the 1 from serial monitor then seperation and relay activated. 
if you send the 2 from serial monitor then seperation and relay deactivated. 
*/

ServoSeperation seperator(HAL::Pin::SEPERATION_SERVO_PIN);
RelaySeperation relay(HAL::Pin::FIRE_SMOKE_PIN);

void setup() {
    Serial.begin(9600);
    seperator.begin();
    relay.begin();
    seperator.set_alt_min(150);
}

void loop() {
    if(Serial.available()) {
        char c = Serial.read();
        Serial.println(c);
        if(c == '1') {
            Serial.println("On");
            seperator.run(150);
            relay.run(150);
        }
        else if(c=='2') {
            Serial.println("Off");
            seperator.close();
            relay.close();
        }
    }
}