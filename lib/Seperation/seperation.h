#ifndef SEPERATION_H
#define SEPERATION_H

#define GU_SEPERATION_SERVO_ON_PWM_DEFAULT      1500    // default PWM value to move servo to when shutter is activated
#define GU_SEPERATION_SERVO_OFF_PWM_DEFAULT     1100    // default PWM value to move servo to when shutter is deactivated

#ifdef DEBUG
    #define GU_SEPERATION_ALT_MIN_DEFAULT       0
#else
    #define GU_SEPERATION_ALT_MIN_DEFAULT       100     // default min altitude the vehicle should have before seperation is released
#endif

#include <Servo.h>
#include "gpio.h"

class Seperation {
public:
    Seperation(HAL::Pin pin);
    
    int alt_min()const { return _alt_min; }
    virtual void set_alt_min(int alt_min) { _alt_min = alt_min; }
    
    // run - release if current altitude is bigger than minimum seperation altitude
    void run();
    void safe_run();
    void close();
    bool enable() { return _enabled; }
    virtual void begin() = 0;
protected:
    ~Seperation() {}
    HAL::Pin _pin;
    int _alt_min;
    bool _enabled = false;
private:
    /// release - release seperation
    virtual void release() = 0;
    /// close - close seperation
    virtual void turn_off() = 0;
};
 
class ServoSeperation : public Seperation{
public:
    ServoSeperation(HAL::Pin pin);

    /* Do not allow copies */
    ServoSeperation(const ServoSeperation&) = delete;
    ServoSeperation& operator=(const ServoSeperation&) = delete;

    virtual void begin() override;

private:
    Servo _seperator;
    virtual void turn_off() override;
    virtual void release() override;
};

class RelaySeperation : public Seperation {
public:
    RelaySeperation(HAL::Pin pin);

    /* Do not allow copies */
    RelaySeperation(const RelaySeperation&) = delete;
    RelaySeperation& operator=(const RelaySeperation&) = delete;

    virtual void begin() override;

private:
    virtual void release() override;
    virtual void turn_off() override;
};

#endif