#include "seperation.h"

#include "pin_util.h"
#include "gazisensor.hpp"

static GaziSensor* _gSensor(GaziSensor::get_singleton());

Seperation::Seperation(HAL::Pin pin) 
    : _pin(pin), _alt_min(GU_SEPERATION_ALT_MIN_DEFAULT)
{

}

// run - release the seperation
void Seperation::run() {
    if(!_enabled) {
        release();
        _enabled = true;
    }
}

// run - release if current altitude is bigger than minimum seperation altitude
void Seperation::safe_run() {
    if(_gSensor->sensor.m_avg_altitude > _alt_min) {
        release();
        _enabled = true;
    }
}

void Seperation::close() {
    if(_enabled) {
        turn_off();
        _enabled = false;
    }
}

//-------------------------------------------------------------
//-------------------------------------------------------------
 
ServoSeperation::ServoSeperation(HAL::Pin pin) : Seperation(pin)
{
    
}

void ServoSeperation::begin() {
    _seperator.attach(static_cast<uint8_t>(_pin));
}

void ServoSeperation::release() {
    _seperator.write(0);
}

void ServoSeperation::turn_off() {
    _seperator.write(180);
}

//-------------------------------------------------------------
//-------------------------------------------------------------

RelaySeperation::RelaySeperation(HAL::Pin pin) : Seperation(pin)
{
    
}

void RelaySeperation::release() {
    digitalWrite(_pin,HAL::PinDigital::HI);
}

void RelaySeperation::begin() {
    pinMode(_pin,HAL::PinMode::OUT);
    digitalWrite(_pin,HAL::PinDigital::LO);
}

void RelaySeperation::turn_off() {
    digitalWrite(_pin,HAL::PinDigital::LO);
}






// Seperation::Seperation(HAL::Pin _pin,Seperator seperator) : _pin(_pin), 
//     _seperator(seperator)
// {
//     switch (_seperator)
//     {
//         case Seperator::SERVO:
//             _servo.attach(static_cast<int>(_pin));
//             break;
//         case Seperator::UNKNOWN:
//         case Seperator::RELAY:    
//             HAL::pinMode(_pin,HAL::PinMode::OUT);
//             HAL::digitalWrite(_pin,HAL::PinDigital::LO);
//             break;
//     }
// }

// /// release - release seperation
// void Seperation::release()
// {
//     _enabled = true;
// }
