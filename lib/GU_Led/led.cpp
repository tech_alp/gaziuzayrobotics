#include "led.h"
#include <Arduino.h>
#include "Notify.h"

//****************************************************************

namespace {
    // Time when color routine must act next time
    uint32_t colorNextToggleTime = 0;

    // If led active and blinking bigger than 0, then led will be off. 
    bool ledIsOn   = false;

    constexpr uint8_t color_red[]          = {255, 0,   0};
    constexpr uint8_t color_green[]        = {0,   255, 0};
    constexpr uint8_t color_blue[]         = {0,   0,   255};
    constexpr uint8_t color_orange[]       = {255, 165, 0};

    constexpr uint8_t color_purple[]       = {128, 0,   128};
    constexpr uint8_t color_bright_green[] = {153, 204, 0};
    constexpr uint8_t color_bright_blue[]  = {0,   204, 255};

    struct LedColorTableEntry_t {
        Led::LedMode_e mode;
        const uint8_t* color;
        const char* name;
        uint8_t blinkingTimes;
    };

    #define LED_COLOR_ENTRY(a,b,c,d) a,b,c,d

    constexpr LedColorTableEntry_t ledColorTable[] = {
        {LED_COLOR_ENTRY(Led::LedMode_e::ARMED_NO_GPS,   color_blue,    "ARMED_NO_GPS", 0) },
        {LED_COLOR_ENTRY(Led::LedMode_e::ARMED_GPS,      color_green,   "ARMED_GPS",    0) },
        {LED_COLOR_ENTRY(Led::LedMode_e::LOW_BATTERY,    color_orange,  "LOW_BATTERY",  0) },
        {LED_COLOR_ENTRY(Led::LedMode_e::READY_NO_GPS,   color_blue,    "READY_NO_GPS", 2) },
        {LED_COLOR_ENTRY(Led::LedMode_e::READY_GPS,      color_green,   "READY_GPS",    2) },
        {LED_COLOR_ENTRY(Led::LedMode_e::ERROR,          color_red,     "ERROR",        5) }
    };

    const LedColorTableEntry_t* currentLedColorEntry = nullptr;
    #define LED_COLOR_ENTRY_COUNT (sizeof(ledColorTable) / sizeof(LedColorTableEntry_t))

} // anonymous namespace

//****************************************************************

Led::Led(uint8_t red_pin, uint8_t green_pin, uint8_t blue_pin)
    : _red_pin(red_pin), _green_pin(green_pin), _blue_pin(blue_pin)
{
    pinMode(_red_pin,OUTPUT);
    pinMode(_green_pin,OUTPUT);
    pinMode(_blue_pin,OUTPUT);
    
}

Led::Led(HAL::Pin red_pin, HAL::Pin green_pin, HAL::Pin blue_pin) 
    : Led(static_cast<uint8_t>(red_pin),static_cast<uint8_t>(green_pin),
    static_cast<uint8_t>(blue_pin))
{
}

void Led::turn_on(uint8_t red_val, uint8_t green_val, uint8_t blue_val)
{
    analogWrite(_red_pin,red_val);
    analogWrite(_green_pin,green_val);
    analogWrite(_blue_pin,blue_val);
    ledIsOn = true;
}

void Led::turn_off() {
    analogWrite(_red_pin,0);
    analogWrite(_green_pin,0);
    analogWrite(_blue_pin,0);
    ledIsOn = false;
}

void Led::stop() {
    analogWrite(_red_pin,0);
    analogWrite(_green_pin,0);
    analogWrite(_blue_pin,0);
    ledIsOn = false;
    currentLedColorEntry = nullptr;
    colorNextToggleTime = 0;
}

void Led::color(uint8_t red_val,uint8_t green_val,uint8_t blue_val)
{
    //if there is currentLedColorEntry, led must stop
    if(currentLedColorEntry){
        stop();
    }
    turn_on(red_val, green_val, blue_val);
}

void Led::color(const uint8_t* rgb_color)
{
    color(rgb_color[0],rgb_color[1],rgb_color[2]);
}

void Led::color(Led::LedColor_e rgb_color)
{
    switch (rgb_color)
    {
    case LedColor_e::BLUE:
        color(color_blue);
        break;
    case LedColor_e::GREEN:
        color(color_green);
        break;
    case LedColor_e::ORANGE:
        color(color_orange);
        break;
    case LedColor_e::PURPLE:
        color(color_purple);
        break;
    case LedColor_e::RED:
        color(color_red);
        break;
    default: //do nothing
        turn_off();
        break;
    }
}

void Led::color(LedMode_e mode)
{
    const LedColorTableEntry_t* selectedCandidate = nullptr;
    for(uint8_t i = 0; i < LED_COLOR_ENTRY_COUNT; i++) {
        const LedColorTableEntry_t* candidate = &ledColorTable[i];
        if(candidate->mode != mode) {
            continue;
        }

        if(!currentLedColorEntry) {
            selectedCandidate = candidate;
        } else if(currentLedColorEntry->mode != mode) {
            selectedCandidate = candidate;
        }

        break;
    }

    if(!selectedCandidate) {
        return;
    }

    currentLedColorEntry = selectedCandidate;
    colorNextToggleTime = 0;
}

// updateColor -> update function called at 50Hz
void Led::colorUpdate(uint32_t currentTime)
{
    if(currentLedColorEntry == nullptr) {
        return;
    }

    if(currentLedColorEntry->blinkingTimes == 0) {
        turn_on(currentLedColorEntry->color[0],currentLedColorEntry->color[1],currentLedColorEntry->color[2]);
        return;
    }

    if(colorNextToggleTime > currentTime) {
        return;
    }

    if(!ledIsOn) {
        turn_on(currentLedColorEntry->color[0],currentLedColorEntry->color[1],currentLedColorEntry->color[2]);
    } else {
        turn_off();
    }

    colorNextToggleTime = currentTime + (uint32_t)(1000/currentLedColorEntry->blinkingTimes);
}

Led status_led(HAL::Pin::STATUS_RED_LED,HAL::Pin::STATUS_GREEN_LED,HAL::Pin::STATUS_BLUE_LED);
