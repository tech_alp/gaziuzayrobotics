#ifndef _LED_H
#define _LED_H

#include <stdint.h>
#include "gpio.h"

#define SLOW_BLINK        500  // half second
#define FAST_BLINK        150  // 4 times per second
#define SUPERFAST_BLINK   100  // 10 times per second 


class Led {
public:
    enum class LedMode_e : uint32_t {
        ARMED_NO_GPS = 0x0,  //blue
        ARMED_GPS    = 0x1,  //green
        LOW_BATTERY  = 0x2,  //orange
        READY_NO_GPS = 0x3,  //blue with blinking
        READY_GPS    = 0x4,  //green with blinking
        ERROR        = 0x5   //red with blinking
    };

    enum LedColor_e {
        BLACK  = 0x0,
        RED    = 0x1,
        GREEN  = 0x2,
        BLUE   = 0x3,
        PURPLE = 0x4,
        ORANGE = 0x5,
        YELLOW = (RED|GREEN),
        WHITE  = (RED|GREEN|BLUE)
    };

    explicit Led(uint8_t red_pin, uint8_t green_pin, uint8_t blue_pin);
    Led(HAL::Pin red_pin, HAL::Pin green_pin, HAL::Pin blue_pin);
    
    void turn_on(uint8_t red_val, uint8_t green_val, uint8_t blue_val);
    void turn_off();
    void stop();
    
    void color(uint8_t red,uint8_t green,uint8_t blue);
    void color(const uint8_t* rgb_color);
    void color(Led::LedColor_e rgb_color);
    void color(LedMode_e status);

    void colorUpdate(uint32_t currentTime);
    void update(uint32_t currentTime);

private:
    uint8_t _red_pin;
    uint8_t _green_pin;
    uint8_t _blue_pin;

    uint8_t brightness{255}; // TODO: get from the user
};

extern Led status_led;

#endif