#include "buzzer.h"

#include <Arduino.h>

#define BEEPER_COMMAND_STOP 0xFF
#define BEEPER_COMMAND_REPEAT 0xFE
#define MAX_MULTI_BEEPS 64   //size limit for 'beep_multiBeeps[]'

/*
ARMING	            3 sec long Beep
ARMING FAILURE	    Single Beep
DISARMED	        Single Beep
BATTERY FAILSAFE	Single Beep repeating every 3 sec
LOST VEHICLE	    Beep-Beep repeating every 3 sec
*/

namespace {

// Place in current sequence
uint16_t beeperPos = 0;
// Time when beeper routine must act next time
uint32_t beeperNextToggleTime = 0;

/* Beeper Sound Sequences: (Square wave generation)
 * Sequence must end with 0xFF or 0xFE. 0xFE repeats the sequence from
 * start when 0xFF stops the sound when it's completed.
 *
 * "Sound" Sequences are made so that 1st, 3rd, 5th.. are the delays how
 * long the beeper is on and 2nd, 4th, 6th.. are the delays how long beeper
 * is off. Delays are in milliseconds/10 (i.e., 5 => 50ms).
 */

// arming beep
const uint8_t beep_armingBeep[] = {
    30, 5, 5, 5, BEEPER_COMMAND_STOP
};
// motor test beep
const uint8_t beep_motorTestBeep[] = {
    50, 10, 50, 10, 50, 10, 50, 10, 50, 10, 50, 10, 50, 10, 50,
    10, 50, 10, 50, 10, 50, 10, 50, 10, 50, 10, 50, 10, 50, 10,
    50, 10, 40, BEEPER_COMMAND_STOP
};
// Arming when GPS is fixed
const uint8_t beep_armingGpsFix[] = {
    5, 5, 15, 5, 5, 5, 15, 30, BEEPER_COMMAND_STOP
};
// Arming when GPS is not fixed
const uint8_t beep_armingGpsNoFix[] = {
    30, 5, 30, 5, 30, 5, BEEPER_COMMAND_STOP
};

// armed beep (first pause, then short beep)
const uint8_t beep_armedBeep[] = {
    0, 245, 10, 5, BEEPER_COMMAND_STOP
};
// disarming beeps
const uint8_t beep_disarmBeep[] = {
    15, 5, 15, 5, BEEPER_COMMAND_STOP
};
// beeps while stick held in disarm position (after pause)
const uint8_t beep_disarmRepeatBeep[] = {
    0, 100, 10, BEEPER_COMMAND_STOP
};
// Long beep and pause after that
 const uint8_t beep_lowBatteryBeep[] = {
    25, 50, BEEPER_COMMAND_REPEAT
};
// critical battery beep
const uint8_t beep_critBatteryBeep[] = {
    50, 2, BEEPER_COMMAND_REPEAT
};

// transmitter-signal-lost tone
const uint8_t beep_txLostBeep[] = {
    50, 50, BEEPER_COMMAND_STOP
};
// SOS morse code:
const uint8_t beep_sos[] = {
    10, 10, 10, 10, 10, 40, 40, 10, 40, 10, 40, 40, 10, 10, 10, 10, 10, 70, BEEPER_COMMAND_STOP
};
// Ready beeps. When gps has fix and copter is ready to fly.
const uint8_t beep_readyBeep[] = {
    4, 5, 4, 5, 8, 5, 15, 5, 8, 5, 4, 5, 4, 5, BEEPER_COMMAND_STOP
};
// 2 fast short beeps
const uint8_t beep_2shortBeeps[] = {
    5, 5, 5, 5, BEEPER_COMMAND_STOP
};
// 2 longer beeps
const uint8_t beep_2longerBeeps[] = {
    20, 15, 35, 5, BEEPER_COMMAND_STOP
};
// 3 beeps
const uint8_t beep_gyroCalibrated[] = {
    20, 10, 20, 10, 20, 10, BEEPER_COMMAND_STOP
};

// when system start up
const uint8_t beep_system_init[] = {
    15, 10, 15, 10, 30, 10, 10, 10, BEEPER_COMMAND_STOP
};

// Cam connection opened
const uint8_t beep_camOpenBeep[] = {
    5, 15, 10, 15, 20, BEEPER_COMMAND_STOP
};

// Cam connection close
const uint8_t beep_camCloseBeep[] = {
    10, 8, 5, BEEPER_COMMAND_STOP
};

// RC Smoothing filter not initialized - 3 short + 1 long
const uint8_t beep_rcSmoothingInitFail[] = {
    10, 10, 10, 10, 10, 10, 50, 25, BEEPER_COMMAND_STOP
};

// array used for variable # of beeps (reporting GPS sat count, etc)
uint8_t beep_multiBeeps[MAX_MULTI_BEEPS + 1];

struct beeperTableEntry_t {
    uint8_t mode;
    uint8_t priority;  //0 highest
    const uint8_t *sequence;
    const char *name;
};

#define BEEPER_ENTRY(a,b,c,d) a,b,c,d

// IMPORTANT: these are in priority order, 0 = Highest
constexpr beeperTableEntry_t beeperTable[] = {
    { BEEPER_ENTRY(Buzzer::BEEPER_GYRO_CALIBRATED,       0, beep_gyroCalibrated,   "GYRO_CALIBRATED") },
    { BEEPER_ENTRY(Buzzer::BEEPER_DISARMING,             3, beep_disarmBeep,       "DISARMING") },
    { BEEPER_ENTRY(Buzzer::BEEPER_MOTOR_TEST,            4, beep_motorTestBeep,    "MOTOR_TEST") },
    { BEEPER_ENTRY(Buzzer::BEEPER_ARMING,                4, beep_armingBeep,       "ARMING")  },
    { BEEPER_ENTRY(Buzzer::BEEPER_ARMING_GPS_FIX,        5, beep_armingGpsFix,     "ARMING_GPS_FIX") },
    { BEEPER_ENTRY(Buzzer::BEEPER_ARMING_GPS_NO_FIX,     6, beep_armingGpsNoFix,   "ARMING_GPS_NO_FIX") },
    { BEEPER_ENTRY(Buzzer::BEEPER_BAT_CRIT_LOW,          7, beep_critBatteryBeep,  "BAT_CRIT_LOW") },
    { BEEPER_ENTRY(Buzzer::BEEPER_BAT_LOW,               8, beep_lowBatteryBeep,   "BAT_LOW") },
    { BEEPER_ENTRY(Buzzer::BEEPER_GPS_STATUS,            9, beep_multiBeeps,       "GPS_STATUS") },
    { BEEPER_ENTRY(Buzzer::BEEPER_ACC_CALIBRATION,       11, beep_2shortBeeps,     "ACC_CALIBRATION") },
    { BEEPER_ENTRY(Buzzer::BEEPER_ACC_CALIBRATION_FAIL,  12, beep_2longerBeeps,    "ACC_CALIBRATION_FAIL") },
    { BEEPER_ENTRY(Buzzer::BEEPER_READY_BEEP,            13, beep_readyBeep,       "READY_BEEP") },
    { BEEPER_ENTRY(Buzzer::BEEPER_DISARM_REPEAT,         15, beep_disarmRepeatBeep, "DISARM_REPEAT") },
    { BEEPER_ENTRY(Buzzer::BEEPER_ARMED,                 16, beep_armedBeep,       "ARMED") },
    { BEEPER_ENTRY(Buzzer::BEEPER_SYSTEM_INIT,           17, beep_system_init,     "SYSTEM_INIT") },
    { BEEPER_ENTRY(Buzzer::BEEPER_ALL,                   24, NULL,                 "ALL") },
};

const beeperTableEntry_t *currentBeeperEntry = NULL;
#define BEEPER_TABLE_ENTRY_COUNT (sizeof(beeperTable) / sizeof(beeperTableEntry_t))

}


Buzzer::Buzzer(uint8_t pin) : _pin(pin), _beeperIsON(false)
{
    pinMode(_pin,OUTPUT);
}

Buzzer::Buzzer(HAL::Pin pin) : Buzzer(static_cast<uint8_t>(pin))
{
}

void Buzzer::beeper(beeperMode_e mode)
{
    const beeperTableEntry_t* selectedCandidate = NULL;
    for(uint8_t i = 0; i < BEEPER_TABLE_ENTRY_COUNT; ++i) {
        const beeperTableEntry_t *candidate = &beeperTable[i];
        if(candidate->mode != mode) {
            continue;
        }

        if(!currentBeeperEntry) {
            selectedCandidate = candidate;
            break;
        }

        if(candidate->priority < currentBeeperEntry->priority) {
            beeperSilence();
            selectedCandidate = candidate;
        }

        break;
    }

    if(!selectedCandidate) {
        return;
    }

    currentBeeperEntry = selectedCandidate;

    beeperPos = 0;
    beeperNextToggleTime = 0;
}

void Buzzer::beeperSilence()
{
    noTone(_pin);
    _beeperIsON = false;

    beeperNextToggleTime = 0;
    beeperPos = 0;

    currentBeeperEntry = nullptr;
}

void Buzzer::beeperUpdate(uint32_t currentTime)
{
    // Beeper routine doesn't need to update if there aren't any sounds ongoing
    if (currentBeeperEntry == NULL) {
        return;
    }

    if (beeperNextToggleTime > currentTime) {
        return;
    }
  
    if (!_beeperIsON) {
        if (currentBeeperEntry->sequence[beeperPos] != 0) {
            if(beeperPos%2==0)
                tone(_pin,1000,currentBeeperEntry->sequence[beeperPos]*100);
            _beeperIsON = true;
        }
    } else {
        if (currentBeeperEntry->sequence[beeperPos] != 0) {
            noTone(_pin);
            _beeperIsON = false;
        }
    }

    beeperProcessCommand(currentTime);
}

/*
 * Calculates array position when next to change beeper state is due.
 */
void Buzzer::beeperProcessCommand(uint32_t currentTime)
{
    if (currentBeeperEntry->sequence[beeperPos] == BEEPER_COMMAND_REPEAT) {
        beeperPos = 0;
    } else if (currentBeeperEntry->sequence[beeperPos] == BEEPER_COMMAND_STOP) {
        beeperSilence();
    } else {
        // Otherwise advance the sequence and calculate next toggle time
        beeperNextToggleTime = currentTime + 10 * currentBeeperEntry->sequence[beeperPos];
        beeperPos++;
    }
}

Buzzer::beeperMode_e Buzzer::beeperModeForTableIndex(int idx)
{
    if(idx >= 0 && idx < (int)BEEPER_TABLE_ENTRY_COUNT)
        return static_cast<beeperMode_e>(beeperTable[idx].mode);
    return BEEPER_SILENCE;
}

const char* Buzzer::beeperNameForTableIndex(int idx)const
{
    return (idx >= 0 && idx < (int)BEEPER_TABLE_ENTRY_COUNT) ? beeperTable[idx].name : NULL;
}

int Buzzer::beeperTableEntryCount()const
{
    return BEEPER_TABLE_ENTRY_COUNT;
}

bool Buzzer::isBeeperOn()const
{
    return _beeperIsON;
}

namespace GU {

    Buzzer buzz(HAL::Pin::BUZZER);
}