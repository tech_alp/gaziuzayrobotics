#ifndef _BUZZER_H
#define _BUZZER_H

#include <stdint.h>
#include "gpio.h"

class Buzzer {
public:
    enum beeperMode_e : uint8_t {
        // IMPORTANT: the order of the elements should be preserved for backwards compatibility with the configurator.
        BEEPER_SILENCE = 0,             // Silence, see beeperSilence()
        BEEPER_MOTOR_TEST,
        BEEPER_GYRO_CALIBRATED,
        BEEPER_DISARMING,               // Beep when disarming the board
        BEEPER_ARMING,                  // Beep when arming the board
        BEEPER_ARMING_GPS_FIX,          // Beep a special tone when arming the board and GPS has fix
        BEEPER_BAT_CRIT_LOW,            // Longer warning beeps when battery is critically low (repeats)
        BEEPER_BAT_LOW,                 // Warning beeps when battery is getting low (repeats)
        BEEPER_GPS_STATUS,              // Use the number of beeps to indicate how many GPS satellites were found
        BEEPER_ACC_CALIBRATION,         // ACC inflight calibration completed confirmation
        BEEPER_ACC_CALIBRATION_FAIL,    // ACC inflight calibration failed
        BEEPER_READY_BEEP,              // Ring a tone when GPS is locked and ready
        BEEPER_DISARM_REPEAT,           // Beeps sounded while stick held in disarm position
        BEEPER_ARMED,                   // Warning beeps when board is armed with motors off when idle (repeats until board is disarmed or throttle is increased)
        BEEPER_SYSTEM_INIT,             // Initialisation beeps when board is powered on
        BEEPER_USB,                     // Some boards have beeper powered USB connected
        BEEPER_ARMING_GPS_NO_FIX,       // Beep a special tone when arming the board and GPS has no fix
        BEEPER_ALL,                     // Turn ON or OFF all beeper conditions
        // BEEPER_ALL must remain at the bottom of this enum
    };

    Buzzer(uint8_t pin);
    explicit Buzzer(HAL::Pin pin);

    void beeper(beeperMode_e mode);

    void beeperUpdate(uint32_t currentTime);

    void beeperSilence();

    beeperMode_e beeperModeForTableIndex(int idx);
    const char *beeperNameForTableIndex(int idx)const;
    int beeperTableEntryCount()const;
    bool isBeeperOn()const;

private:
    uint8_t _pin;
    bool _beeperIsON;
    void beeperProcessCommand(uint32_t currentTimeUs);
};

namespace GU {

extern Buzzer buzz;

}

#endif