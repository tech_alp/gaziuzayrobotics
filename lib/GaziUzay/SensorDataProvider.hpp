#ifndef _SENSORDATAPROVIDER_HPP
#define _SENSORDATAPROVIDER_HPP

#include <stdint.h>
#include <WString.h>

class SensorDataProvider {
public:
    SensorDataProvider();
	
    const String& DateTime() const;
	int NumofPackage() const;
	float Pressure() const;
	float Altitude() const;
	float Velocity() const;
	float Temperature() const;
	float Voltage() const;
	float GpsLatitude() const;
	float GpsLongitude() const;
	float GpsAltitude() const;
	int StatusSystem() const;	
	int Pitch() const;
	int Roll() const;
	int Yaw() const;
	int NumofTurns() const;
	bool VideoInfo() const;

    virtual void set_dateTime() = 0;
	virtual void set_numofPackage() = 0;
	virtual void set_pressure() = 0;
	virtual void set_altitude() = 0;
	virtual void set_velocity() = 0;
	virtual void set_temperature() = 0;
	virtual void set_voltage(uint8_t pin) = 0;
	virtual void set_gpsLatitude() = 0;
	virtual void set_gpsLongitude() = 0;
	virtual void set_gpsAltitude() = 0;
	virtual void set_statusSystem() = 0;
	virtual void set_axisActions() = 0;
	virtual void set_pitch() = 0;
	virtual void set_roll() = 0;
	virtual void set_yaw() = 0;
	virtual void set_numofTurns() = 0;
	virtual void set_videoInfo(bool) = 0;

protected:
    // Variables defined for BME280
	float m_pressure{};
	float m_temperature{};
	float m_altitude{};
	float m_velocity{};
	
    // Variables defined for BNO055
	int m_pitch{};
	int m_roll{};
	int m_yaw{};

    // Variables defined for GPS
	float m_gps_latitude{};
	float m_gps_longitude{};
	float m_gps_altitude{};

    // Independent variables
	float m_voltage{};
	int m_team_number{};
    int m_numofpackage{};
	int m_statusSystem{};
    int m_numofturns{};
    bool m_videoInfo{};

    String m_dateTime{};
};

#endif