//           ░██████╗░░█████╗░███████╗██╗  ██╗░░░██╗███████╗░█████╗░██╗░░░██╗
//          ██╔════╝░██╔══██╗╚════██║██║  ██║░░░██║╚════██║██╔══██╗╚██╗░██╔╝
//         ██║░░██╗░███████║░░███╔═╝██║  ██║░░░██║░░███╔═╝███████║░╚████╔╝░
//        ██║░░╚██╗██╔══██║██╔══╝░░██║  ██║░░░██║██╔══╝░░██╔══██║░░╚██╔╝░░
//       ╚██████╔╝██║░░██║███████╗██║  ╚██████╔╝███████╗██║░░██║░░░██║░░░
//      ░╚═════╝░╚═╝░░╚═╝╚══════╝╚═╝  ░╚═════╝░╚══════╝╚═╝░░╚═╝░░░╚═╝░░░

#include "gaziuzay.hpp"

#include <Servo.h>
#include <Adafruit_BNO055.h>
#include <Adafruit_BME280.h>
#include <Adafruit_Sensor.h>
#include <utility/imumaths.h>

GaziUzay* GaziUzay::_singleton;

// ----------------------------------------------------------------------------
GaziUzay::GaziUzay()
{
    if(_singleton) {
        //There is a existing motor instance
        //TODO return error
    }
    _singleton = this;
}

// ----------------------------------------------------------------------------
GaziUzay& GaziUzay::setSensor(void* sensor,GaziUzay::SensorType sensorName)
{
    switch(sensorName)
    {
        // ------------------------------------------------------------------------
        case SensorType::G_BNO055: {
            //g_bno.bno = reinterpret_cast<Adafruit_BNO055*>(sensor);
            m_bno = reinterpret_cast<Adafruit_BNO055*>(sensor);
            initBNO();
            break;
        }
        
        // ------------------------------------------------------------------------
        case SensorType::G_BME280: {
            // g_bno.bme = reinterpret_cast<Adafruit_BME280*>(sensor);
            m_bme = reinterpret_cast<Adafruit_BME280*>(sensor);
            initBME();
            break;
        }

        // ------------------------------------------------------------------------
        // TODO exeption handling on arduino serial port!
        default:
            Serial.println(sensorName + " Not Found!");
    }
    return *this;
}

// ----------------------------------------------------------------------------
//Setup function for the BME280 sensor.
void GaziUzay::initBME()
{
    if (!m_bme->begin())
	{
        #if(DEBUG)
            Serial.println("The BME280 sensor could not be initialized. Check your wiring or I2C ADDR!");
        #endif
        g_bme.sensorState = Error;
    }
    else
    {
        g_bme.sensorState = Connecting;
    }
}

// ----------------------------------------------------------------------------
//Initiate function for the BNO055 sensor.
void GaziUzay::initBNO()
{
	if(!m_bno->begin()) 
    {
        #if(DEBUG)
            Serial.println("The BNO055 sensor could not be initialized. Check your wiring or I2C ADDR!");
            
        #endif
        g_bno.sensorState = Error;
	}
    else
    {
	    // Use external crystal for better accuracy.
	    m_bno->setExtCrystalUse(true);
        g_bno.sensorState = Connecting;
    }
}

// ----------------------------------------------------------------------------
//// Initialize function for alternative gps sensor GPS NEO.
void GaziUzay::initGPS()
{
// 	gps.startSerial(_baud);
// 	delay(1000);
// 	gps.setSentencesToReceive(OUTPUT_GGA); // We are using GGA protocol.
}

// ----------------------------------------------------------------------------
//// Calibration function for initial settings.
void GaziUzay::altitudeCalibration()
{
    m_baselinePressure = m_bme->readPressure();
}


// ----------------------------------------------------------------------------
void GaziUzay::PrintSensorDetails(const Gazi_Sensor_t& sensor) const
{
    switch (sensor.sensorState)
    {
        #define X(ID) case SensorType::ID: \
            Serial.println(F("------------------------------------")); \
            Serial.print(F("Sensor:       ")); \
            Serial.println(sensor.name); \
            Serial.print(F("Type:         ")); \
            Serial.print(#ID); \
            Serial.print(F("Sensor State:       ")); \
            Serial.print(sensor.sensorState); \
            Serial.print(F("Time Stamp:       ")); \
            Serial.print(sensor.timestamp); \
            Serial.println(F("------------------------------------\n"));

            XSENSORTYPE
        #undef X
    }
    Serial.print(F("Sensor Details Not Found!"));
}

// ----------------------------------------------------------------------------
void GaziUzay::set_dateTime() 
{
    // String secondDig, minuteDig, hourDig, yearDig, monthDig, dayDig;
    // if(second() < 10)
	// secondDig = ( "0" + String(second()) );
	// else
	// secondDig = ( String(second()) );

	// if(minute() < 10)
	// minuteDig = ( "0" + String(minute()) );
	// else
	// minuteDig = ( String(minute()) );

	// if(hour() < 10)
	// hourDig = ( "0" + String(hour()) );
	// else
	// hourDig = ( String(hour()) );

	// yearDig = ( String(year()) );

	// if(month() < 10)
	// monthDig = ( "0" + String(month()) );
	// else
	// monthDig = ( String(month()) );

	// if(day() < 10)
	// dayDig = ( "0" + String(day()) );
	// else

	// dayDig = ( String(day()) );

    // m_dateTime = ( dayDig   + "/" + monthDig + "/" + yearDig + " " + hourDig + ":" + minuteDig + ":" + secondDig);

    // ********************************* // ********************************* // ********************************* //

    // Kalibrasyon ile alınan zaman verileri EEPROM.put(missionTimeAdress[i],missionTime[i]); ile EEPROM'a koyulacak.
    // Ardından kalibrasyon kısmında gönderim saati çağrılırken aşağıdaki kod yazılacak.
    // setTime(missionTime[0], missionTime[1], missionTime[2], missionTime[3], missionTime[4], missionTime[5]);
    // setTime(hours, minutes, seconds, days, months, years);

    // ********************************* // ********************************* // ********************************* //

    // Aşağıdaki kısım ise set_dateTime() fonksiyonun sonunda verilerin EEPROM'a kayıtlarını yapmak için bulunacak.
    //EEPROM.put(missionTimeAdress[0],hour());
	//EEPROM.put(missionTimeAdress[1],minute());
	//EEPROM.put(missionTimeAdress[2],second());
    //EEPROM.put(missionTimeAdress[3],day());
	//EEPROM.put(missionTimeAdress[4],month());
	//EEPROM.put(missionTimeAdress[5],year());
}

// ----------------------------------------------------------------------------
void GaziUzay::set_numofPackage()
{
    m_numofpackage = m_numofpackage + 1;
}

// ----------------------------------------------------------------------------
void GaziUzay::set_pressure() 
{
    m_pressure = m_bme->readPressure() / 100.0F;
}

// ----------------------------------------------------------------------------
void GaziUzay::set_altitude() 
{
    m_altitude = (44330.0*(1-pow(m_pressure/m_baselinePressure,1/5.255)));
    
    if(m_altitude < 0)	// If altitude is measured below zero due to margin of error, the altitude is considered zero.
	{
	    m_altitude = 0;
	}
}

// ----------------------------------------------------------------------------
void GaziUzay::set_velocity() 
{
    unsigned long currentTime = millis(); // This millis function defined for set_velocity() function.

    if((currentTime - m_previousTime) >= 1000)
    {
    	float timeDifference		= (float) (	(currentTime - m_previousTime) / 1000 );		
	    float altitudeDifference	= (m_altitude - m_previousAltitude);
		
	    // Altitude difference divided by time difference gives us the velocity value.
	    m_velocity = (altitudeDifference / timeDifference);
		
	    // Identification processes.				
	    m_previousTime = currentTime;	// Define for the previous time variable value.
	    m_previousAltitude = m_altitude;	// Define for the previous altitude variable value.
    }
}

// ----------------------------------------------------------------------------
void GaziUzay::set_temperature() 
{
    m_temperature = m_bme->readTemperature();
}

// ----------------------------------------------------------------------------
void GaziUzay::set_voltage(uint8_t pin) 
{
    uint16_t measuredValue = analogRead(pin);
    m_voltage = (float)(((3.3f*measuredValue)/1023.f)*22.86f/5.01f);
}

// ----------------------------------------------------------------------------
void GaziUzay::set_gpsLatitude() 
{
    
}

// ----------------------------------------------------------------------------
void GaziUzay::set_gpsLongitude()
{

}

// ----------------------------------------------------------------------------
void GaziUzay::set_gpsAltitude() 
{

}

// ----------------------------------------------------------------------------
void GaziUzay::set_statusSystem()
{
    
}

// ----------------------------------------------------------------------------
void GaziUzay::set_axisActions()
{
    imu::Quaternion quat = m_bno->getQuat();
    double yy = quat.y() * quat.y();

    double pitchRad = asin (2 *  quat.w() * quat.y() - quat.x() * quat.z());
    double rollRad  = atan2(2 * (quat.w() * quat.x() + quat.y() * quat.z()),1 - 2*(quat.x() * quat.x() + yy));
    double yawRad   = atan2(2 * (quat.w() * quat.z() + quat.x() * quat.y()), 1 - 2*(yy+quat.z() * quat.z()));

    m_pitch = 57.2958 * pitchRad;
    m_roll  = 57.2958 * rollRad;
    m_yaw   = 57.2958 * yawRad;
}

// ----------------------------------------------------------------------------
void GaziUzay::set_pitch() 
{
    imu::Quaternion quat = m_bno->getQuat();
    double pitchRad = asin (2 *  quat.w() * quat.y() - quat.x() * quat.z());
    
    m_pitch = 57.2958 * pitchRad;
}

// ----------------------------------------------------------------------------
void GaziUzay::set_roll() 
{
    imu::Quaternion quat = m_bno->getQuat();
    double yy = quat.y() * quat.y();
    double rollRad  = atan2(2 * (quat.w() * quat.x() + quat.y() * quat.z()),1 - 2*(quat.x() * quat.x() + yy));
    
    m_roll  = 57.2958 * rollRad;
}

// ----------------------------------------------------------------------------
void GaziUzay::set_yaw() 
{
    imu::Quaternion quat = m_bno->getQuat();
    double yy = quat.y() * quat.y();
    double yawRad   = atan2(2 * (quat.w() * quat.z() + quat.x() * quat.y()), 1 - 2*(yy+quat.z() * quat.z()));

    m_yaw   = 57.2958 * yawRad;
}

// ----------------------------------------------------------------------------
void GaziUzay::set_numofTurns() 
{
    if(m_yaw > m_prevYaw ) // Bu kısım için prev_yaw tanımlanmalı.
	{
        if((m_yaw-m_prevYaw) > 180)
            ++m_numofturns;
	}	
	else
	{
        if((m_yaw-m_prevYaw) < -180)
            --m_numofturns;
    }
	
    m_prevYaw = m_yaw; // Previous yaw angle value assignment.
}

// ----------------------------------------------------------------------------
void GaziUzay::set_videoInfo(bool info) 
{
    m_videoInfo = info;
}
