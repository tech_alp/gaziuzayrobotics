//           ░██████╗░░█████╗░███████╗██╗  ██╗░░░██╗███████╗░█████╗░██╗░░░██╗
//          ██╔════╝░██╔══██╗╚════██║██║  ██║░░░██║╚════██║██╔══██╗╚██╗░██╔╝
//         ██║░░██╗░███████║░░███╔═╝██║  ██║░░░██║░░███╔═╝███████║░╚████╔╝░
//        ██║░░╚██╗██╔══██║██╔══╝░░██║  ██║░░░██║██╔══╝░░██╔══██║░░╚██╔╝░░
//       ╚██████╔╝██║░░██║███████╗██║  ╚██████╔╝███████╗██║░░██║░░░██║░░░
//      ░╚═════╝░╚═╝░░╚═╝╚══════╝╚═╝  ░╚═════╝░╚══════╝╚═╝░░╚═╝░░░╚═╝░░░

#ifndef GAZIUZAY_HPP_
#define GAZIUZAY_HPP_

#include "SensorDataProvider.hpp"
#include <stdint.h>
#include "Filter.h"

#define XSENSORTYPE \
X(G_BNO055) \
X(G_BME280) \
X(G_TINYGPS)

// Forward decleration
class Adafruit_BNO055;
class Adafruit_BME280;

class GaziUzay : public SensorDataProvider
{
    friend class Modeluydu;

public:
    
    enum SensorType {
        #define X(ID) ID,
            XSENSORTYPE
        #undef X
    };

    enum SensorState {
        NotFound = 0,
        Connecting,
        Disconnecting,
        Error
    };

    struct Gazi_Sensor_t
    {
        char name[12];
        int32_t type;
        int32_t sensorState;
        int32_t timestamp;
    };

    GaziUzay();
    ~GaziUzay() = default;

    static GaziUzay& getSingleton() {
        return *_singleton;
    }

    GaziUzay& setSensor(void* sensor,SensorType sensorName);

    void initBME();
    void initBNO();
    void initGPS();

    void altitudeCalibration();

    void PrintSensorDetails(const Gazi_Sensor_t& sensorType) const;


    // virtual func
    void set_dateTime() override;
	void set_numofPackage() override;
	void set_pressure() override;
	void set_altitude() override;
	void set_velocity() override;
	void set_temperature() override;
	void set_voltage(uint8_t pin) override;
	void set_gpsLatitude()  override;
	void set_gpsLongitude() override;
	void set_gpsAltitude()  override;
	void set_statusSystem() override;
    void set_axisActions() override;
    void set_pitch() override;
	void set_roll() override;
	void set_yaw() override;
	void set_numofTurns() override;
	void set_videoInfo(bool) override;

    const Gazi_Sensor_t& getG_BNO() const {return g_bno; }
    const Gazi_Sensor_t& getG_BME() const {return g_bme; }

private:
    static GaziUzay* _singleton;
    Gazi_Sensor_t g_bno{"BNO055",G_BNO055,NotFound,0};
    Gazi_Sensor_t g_bme{"BME280",G_BME280,NotFound,0};
    
    Adafruit_BME280* m_bme{nullptr};
    Adafruit_BNO055* m_bno{nullptr};

    // Required variables for some functions.
    float m_baselinePressure{}; // For 'set_altitude()' function.
    float m_previousAltitude{}; // For 'set_velocity()' function.
    uint32_t m_previousTime{};  // Bunun başlangıçta sıfır olarak kabul edilmesi lazım. // For 'set_velocity()' function.
    int m_prevYaw{};  // For 'set_numofTurns()' function.
    float m_maxAltitude;
    float m_apogee;
};

#endif