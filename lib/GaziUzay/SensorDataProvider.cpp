#include "SensorDataProvider.hpp"

SensorDataProvider::SensorDataProvider() {

}

const String& SensorDataProvider::DateTime() const
{
    return m_dateTime;
}

int SensorDataProvider::NumofPackage() const
{
    return m_numofpackage;
}

float SensorDataProvider::Pressure() const
{
    return m_pressure;
}

float SensorDataProvider::Altitude() const
{
    return m_altitude;
}

float SensorDataProvider::Velocity() const
{
    return m_velocity;
}

float SensorDataProvider::Temperature() const
{
    return m_temperature;
}

float SensorDataProvider::Voltage() const
{
    return m_voltage;
}

float SensorDataProvider::GpsLatitude() const
{
    return m_gps_latitude;
}

float SensorDataProvider::GpsLongitude() const
{
    return m_gps_longitude;
}

float SensorDataProvider::GpsAltitude() const
{
    return m_gps_altitude;
}

int SensorDataProvider::StatusSystem() const
{
    return m_statusSystem;
}

int SensorDataProvider::Pitch() const
{
    return m_pitch;
}

int SensorDataProvider::Roll() const
{
    return m_roll;
}

int SensorDataProvider::Yaw() const
{
    return m_yaw;
}

int SensorDataProvider::NumofTurns() const
{
    return m_numofturns;
}

bool SensorDataProvider::VideoInfo() const
{
    return m_videoInfo;
}

// void SensorDataProvider::set_dateTime(const String& dateTime)
// {
//     m_dateTime = dateTime;
// }

// void SensorDataProvider::set_numofPackage(int numofPackage)
// {
//     m_numofpackage = numofPackage;
// }

// void SensorDataProvider::set_pressure(float pressure)
// {
//     m_pressure = pressure;
// }

// void SensorDataProvider::set_altitude(float altitude)
// {
//     m_altitude = altitude;
// }

// void SensorDataProvider::set_velocity(float velocity)
// {
//     m_velocity = velocity;
// }

// void SensorDataProvider::set_temperature(float temperature)
// {
//     m_temperature = temperature;
// }

// void SensorDataProvider::set_voltage(float voltage)
// {
//     m_voltage = voltage;
// }

// void SensorDataProvider::set_gpsLatitude(float lattitude)
// {
//     m_gps_latitude = lattitude;
// }

// void SensorDataProvider::set_gpsLongitude(float longitude)
// {
//     m_gps_longitude = longitude;
// }

// void SensorDataProvider::set_gpsAltitude(float altitude)
// {
//     m_altitude = altitude;
// }

// void SensorDataProvider::set_statusSystem(int statusSystem)
// {
//     m_statusSystem = statusSystem;
// }

// void SensorDataProvider::set_pitch(int pitch)
// {
//     m_pitch = pitch;
// }

// void SensorDataProvider::set_roll(int roll)
// {
//     m_roll = roll;
// }

// void SensorDataProvider::set_yaw(int yaw)
// {
//     m_yaw = yaw;
// }

// void SensorDataProvider::set_numofTurns(int numofTurns)
// {
//     m_numofturns = numofTurns;
// }

// void SensorDataProvider::set_videoInfo(bool videoInfo)
// {
//     m_videoInfo = videoInfo;
// } 
