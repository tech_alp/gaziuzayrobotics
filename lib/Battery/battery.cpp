#include <Arduino.h>

#include "battery.h"
#include "pin_util.h"

/*Constants------------------------------------------------------------*/
#define MINIMUM_BATTERY_VOLTAGE 10
#define LOW_BATTERY_VOLTAGE 11

#define R1 44170 // Nominal: 44200. Higher is larger range.
#define R2 15150 // Nominal: 16000. Lower is larger range.
// Maximum voltage = 3.3*(R1+R2)/R2

// copied from Teensy source. Doesn't seemd to be used anywhere else, so
// temporaryily put this here.
// TODO - put in utility file? TODO this doesn't do float math properly - need
// to cast first
constexpr static float range_map(int x, int in_min, int in_max, int out_min, int out_max) {
    return static_cast<float>((x - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min;
}

Battery* Battery::_singleton;

Battery::Battery(HAL::Pin batterySensorPin) : _divider(static_cast<float>(R2) / (R1 + R2)),
    _batterySensorPin(batterySensorPin)
{
    if(_singleton != nullptr) {
        //TODO error code
        Serial.println("Battery must be singleton!");
    }
    _singleton = this;
}


float Battery::getVoltage()const {
    int inputValue = HAL::analogRead(_batterySensorPin);
    // map it to the range the analog out:
    // 3300mV is the highest voltage Teensy can read.
    float teensyVoltage = range_map(inputValue, 0, 1023, 0, 3300);
    // converts output value from mV to V and divides by voltage divider
    // value to calculate battery input voltage.
    float batteryVoltage = (teensyVoltage / _divider) / 1000;
    return batteryVoltage;
}

Battery::BatteryStatus Battery::getStatus()const {
    float voltage = getVoltage();
    if (voltage < MINIMUM_BATTERY_VOLTAGE)
        return BatteryStatus::CRITICAL_FAILURE;
    else if (voltage < LOW_BATTERY_VOLTAGE)
        return BatteryStatus::NONCRITICAL_FAILURE;
    else
        return BatteryStatus::NOMINAL;
}