#ifndef BATTERY_H
#define BATTERY_H

#include <Arduino.h>
#include "gpio.h"

class Battery {
public:
    enum class BatteryStatus {
        NOMINAL = 0x00,
        NONCRITICAL_FAILURE = 0x01,
        CRITICAL_FAILURE = 0x03
    };

    Battery(HAL::Pin batterySensorPin);

    static Battery& getSingleton() { return *_singleton; }
    
     /**
     * @brief  Gets the battery voltage level. Maximum voltage readable
     *          is 12.0 volts; to adjust, change the resistors used
     *          in the voltage divider.
     * @param  None
     * @return float batteryVoltage - voltage of the battery, in volts.
     */
    float getVoltage()const;

    BatteryStatus getStatus()const;

private:
    static Battery* _singleton;
    float _divider;
    HAL::Pin _batterySensorPin;
};

#endif