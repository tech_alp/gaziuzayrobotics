#ifndef MOTOR_H
#define MOTOR_H

#include <stdio.h>
#include <Servo.h>

#define MOTOR_YAW_FACTOR_CW    -1
#define MOTOR_YAW_FACTOR_CCW    1

#define MOTOR_MIN_SPEED 0
#define MOTOR_MAX_SPEED 255
#define MOTOR_TEST_SPEED 70

#define ESC_MIN_THROTTLE        1000
#define ESC_MAX_THROTTLE        2000
#define ESC_ARM_SIGNAL          1000
#define ESC_ARM_TIME            2000

#define MIN_PWM_VALUE 1000
#define MAX_PWM_VALUE 1980
#define FLUCTUATION_TOLERANCE 9
#define VARIATION_TOLERANCE 200
#define AV_TOLERANCE 200
#define CONTROLLABILITY_THRESHOLD 1080

class Motor {
public:
    Motor(uint8_t pwm_pin,uint16_t min_throttle = ESC_MIN_THROTTLE,
          uint16_t max_throttle = ESC_MAX_THROTTLE);

    /* Do not allow copies */
    Motor(const Motor&) = delete;
    Motor& operator=(const Motor&) = delete;

    // init - intialize the motor
    void init();
    
    // get singleton
    static Motor& get_singleton() {
        return *_singleton;
    }

    // output - sends commands to the motors
    void output();
    // output_min - sends minimum values out to the motors
    void output_min();

    void motor_output_test();

    void armed(bool arm);
    bool armed()const { return _armed; }

    bool onForward();
    bool onReverse();
    
    void setSpeed(int speed);
    int getSpeed()const { return _speed; }

    void motorTestOutput();
    void motorStop();


private:
    static Motor* _singleton;
    uint8_t _pwm_pin;
    Servo _esc;
    bool _armed;
    int _speed;
    int16_t _min_throttle;
    int16_t _max_throttle;
};

#endif