#include "motor.h"
#include <Arduino.h>
#include "util.hpp"
#include "Notify.h"
#include "Buzzer.h"

static uint32_t motorRunTimeMs = 10000;
static uint32_t current_time = 0;
static bool     isMotorTestStart = false;
// singleton instance
Motor* Motor::_singleton;

Motor::Motor(uint8_t pwm_pin, uint16_t min_throttle, uint16_t max_throttle)
    : _pwm_pin(pwm_pin), _min_throttle(min_throttle), _max_throttle(max_throttle)
{
    if(_singleton) {
        //There is a existing motor instance
        //TODO return error
    }
    _singleton = this;
}


void Motor::init()
{
    _esc.attach(_pwm_pin,_min_throttle,_max_throttle);
    _esc.writeMicroseconds(ESC_ARM_SIGNAL);
    unsigned long now = millis();
    while (millis() < now + ESC_ARM_TIME)
    { 
    }
}

void Motor::armed(bool arm)
{
    _armed = arm;
    Notify::flags.armed = _armed;
    if(!_armed) {
        GU::buzz.beeper(Buzzer::beeperMode_e::BEEPER_ARMED);
    } else {
        GU::buzz.beeper(Buzzer::beeperMode_e::BEEPER_DISARMING);
    }
}

void Motor::setSpeed(int speed)
{
    _speed = satu(speed,MOTOR_MAX_SPEED,MOTOR_MIN_SPEED);
}

void Motor::output()
{
    if(!armed()) {
        return;
    }
    if(isMotorTestStart) {
        if(millis() > current_time+motorRunTimeMs) {
            isMotorTestStart = false;
            motorStop();
        }
    }
    int val = map(_speed,MOTOR_MIN_SPEED,MOTOR_MAX_SPEED,ESC_MIN_THROTTLE,ESC_MAX_THROTTLE);
    _esc.writeMicroseconds(val);
}

void Motor::motorTestOutput()
{
    if(!isMotorTestStart) {
        setSpeed(MOTOR_TEST_SPEED);
        current_time = millis();
        isMotorTestStart = true;
    }
}

void Motor::motorStop()
{
    setSpeed(MOTOR_MIN_SPEED);
}