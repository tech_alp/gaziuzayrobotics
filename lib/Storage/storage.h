#ifndef STORAGE_H
#define STORAGE_H

#include <EEPROM.h>

struct EEPROMSTORAGE              /**** EEPROM STORAGE STRUCTURE ****/
{
    //stuff in here gets stored to the EEPROM
    unsigned short count;         /* A count of values stored */
    char           str[10];       /* A string label */
    int            values[100];   /* Array of values collected */
    bool           complete;      /* a flag for whether gathering is finished */
};

struct DataStorage {
    //stuff in here gets stored to the EEPROM
    unsigned short count;         /* A count of values stored */
    char           str[10];       /* A string label */
    int            values[100];   /* Array of values collected */
    bool           complete;      /* a flag for whether gathering is finished */
};

class Storage {
public:
    enum class StorageType_e {
        EEPROM,
        SDCARD
    };
    Storage(StorageType_e type);

private:
    StorageType_e _type;
};

class SDCardStorage {
public:
    SDCardStorage();
    void write();

};


#endif