#include "GU_Baro.h"

GU_Baro::GU_Baro()
{
}

GU_Baro::~GU_Baro()
{
}

void GU_Baro::update()
{
    updatePressure();
    updateTemperature();
    updateHumidity();
    
    update_healty_flag();
}

void GU_Baro::update_healty_flag()
{
    const uint32_t now = millis();

    healthy = (now - last_update_ms < BARO_TIMEOUT_MS) &&
    (now - last_change_ms  < BARO_DATA_CHANGE_TIMEOUT_MS);

    if(temperature < -200 || temperature > 200) {
        healthy = false;
    }
}