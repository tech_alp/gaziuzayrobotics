#include "GU_InertialSensor.h"

GU_InertialSensor::GU_InertialSensor()
{
    m_singleton = this;
}

GU_InertialSensor* GU_InertialSensor::get_singleton()
{
    return m_singleton;
}