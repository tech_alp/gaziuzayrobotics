#ifndef GU_BARO_H
#define GU_BARO_H

// timeouts for health reporting
#define BARO_TIMEOUT_MS                 500     // timeout in ms since last successful read
#define BARO_DATA_CHANGE_TIMEOUT_MS     2000    // timeout in ms since last successful read that involved temperature of pressure changing

#include <Arduino.h>


class GU_Baro
{
    friend class GaziUzay;

public:
    GU_Baro();
    virtual ~GU_Baro();

    virtual void calibrate() = 0;
    virtual bool begin() = 0;
    virtual bool init() = 0;
    virtual void updatePressure() = 0;
    virtual void updateTemperature() = 0;
    virtual void updateHumidity() = 0;

    void update();
    bool healty();

    float get_altitude()const { return altitude; }
    float get_pressure()const { return pressure; }
    float get_temperature()const { return temperature; }

    void update_healty_flag();
    

private:
    uint32_t last_update_ms{};        // last update time in ms
    uint32_t last_change_ms{};        // last update time in ms that included a change in reading from previous readings
    float pressure{};                 // pressure in Pascal
    float temperature{};              // temperature in degrees C
    float altitude{};                 // calculated altitude
    float ground_pressure{};
    float p_correction{};
    bool healthy{};                   // true if sensor is healthy
    bool alt_ok{};                    // true if calculated altitude is ok
    bool calibrated{};                // true if calculated calibrated successfully
};

#endif