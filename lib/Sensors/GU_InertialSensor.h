#ifndef GU_INERTIALSENSOR_H
#define GU_INERTIALSENSOR_H

#define INS_MAX_INSTANCES 1 //Just one instance

#define INS_ACCEL_FREQ    100
#define INS_GYRO_FREQ     100
#define INS_MAG_FREQ      100

#include <Arduino.h>

class GU_InertialSensor {
public:
    GU_InertialSensor();

    GU_InertialSensor(const GU_InertialSensor&) = delete;
    GU_InertialSensor& operator=(const GU_InertialSensor&) = delete;

    static GU_InertialSensor* get_singleton();

    virtual void calibrate();
    virtual void update();
    virtual void init();
    virtual bool healty();

    float get_roll()const;
    float get_pitch()const;
    float get_yaw()const;

    void update_healty_flag();

    void updateAccel();
    void updateGyro();
    void updateMag();

private:
    friend class GaziUzay;
    
    static GU_InertialSensor* m_singleton;
    
    uint32_t last_update_ms{};        // last update time in ms
    uint32_t last_change_ms{};        // last update time in ms that included a change in reading from previous readings
    float roll{};                     // pressure in Pascal
    float pitch{};                    // pitch in degrees C
    float yaw{};                      // calculated altitude
    float ground_pressure{};
    float p_correction{};
    bool healthy{};                   // true if sensor is healthy
    bool alt_ok{};                    // true if calculated altitude is ok
    bool calibrated{};                // true if calculated calibrated successfully
         
};

#endif