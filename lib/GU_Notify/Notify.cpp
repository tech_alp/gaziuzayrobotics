#include "Notify.h"
#include <Arduino.h>

Notify *Notify::_singleton;

Notify::Notify() {
    if(_singleton != nullptr) {
        //TODO: print the error
        Serial.println("AP_Notify must be singleton");
    }
    _singleton = this;
}

struct Notify::notify_flags_and_values_type Notify::flags;

// initialisation
void Notify::init(void)
{
    // clear all flags
    memset(&Notify::flags, 0, sizeof(Notify::flags));
}

namespace GU {

Notify& notify() {
    return *Notify::get_singleton();
}

}