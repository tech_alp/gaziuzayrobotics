#ifndef _NOTIFY_DEVICE_H
#define _NOTIFY_DEVICE_H

class Notify;

class NotifyDevice {
public:
    NotifyDevice() = default;
    virtual ~NotifyDevice() = default;

    virtual bool init() = 0;

    virtual void update() = 0;

    // this pointer is used to read the parameters relative to devices
    const Notify *pNotify;
};

#endif // NOTIFY_DEVICE