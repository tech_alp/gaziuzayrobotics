#ifndef _NOTIFY_H
#define _NOTIFY_H

#include <stdint.h>

class Notify {
public:
    Notify();

    static Notify* get_singleton() {
        return _singleton;
    }

    /* Do not allow copies */
    Notify(const Notify&) = delete;
    Notify& operator=(const Notify&) = delete;

    struct notify_flags_and_values_type {
        bool initialising;        // true if initialising and the vehicle should not be moved
        bool error;
        uint8_t gps_num_sats;     // number of sats
        bool gps_glitching;       // true if the GPS is believed to be glitching is affecting navigation accuracy
        uint8_t flight_mode;      // flight mode
        bool armed;               // 0 = disarmed, 1 = armed
        bool flying;              // 0 = not flying, 1 = flying/ascend/descend
        bool esc_calibration;     // true if calibrating escs
        bool failsafe_battery;    // true if battery failsafe
        bool parachute_release;   // true if parachute is being released
        bool video_recording;     // true when the vehicle is recording video
    };

    static struct notify_flags_and_values_type flags;

    // initialisation
    void init(void);

private:
    static Notify* _singleton;
};

namespace GU {
Notify& notify();
}


#endif //NOTIFY