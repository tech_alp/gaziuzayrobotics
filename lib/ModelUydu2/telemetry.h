#ifndef TELEMETRY_H
#define TELEMETRY_H

#include <Arduino.h>
#include "GU_Baro.h"

// // ----------------------------------------------------------------------------
// enum RequestCommand : uint8_t {
//   UNKNOWN = 0,
//   IMPELLENT,
//   SEPARATION,
//   DATATRANSFER,
//   VIDEOINFO
// };

struct Baro_Telem_t {  
};

struct Attitude_Telem_t {
};

struct Telemetry {
    uint16_t team_number;
    uint32_t packet_number;
    String sending_time;     //Gün/Ay/Yıl, Saat/Dakika/Saniye
    float pressure;
    float altitude;
    float landing_velocity;  //Birimi m/s'dir
    int temperature;
    float battery_voltage;
    float gps_lat;
    float gps_lon;
    float gps_alt;
    uint8_t stallite_status; //(Beklemede, Yükselme, Model Uydu İniş, Ayrılma, Görev Yükü İniş, Kurtarma vs. gibi)
    float pitch;             //Birimi derecedir.
    float roll;
    float yaw;
    int turn_number;
    bool video_transfer_info;
};

#endif