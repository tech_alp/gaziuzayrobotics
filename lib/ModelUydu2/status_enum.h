#ifndef STATUS_ENUM_H
#define STATUS_ENUM_H

#include <stdint.h>

enum class StateId_e {
    LAUNCHPAD,                //fırlatma rampası
    ASCENT,                   //yükseliş
    APOGEE,                   //zirve
    DESCENT,                  //iniş,alçalma
    SEPERATION,               //ayrılma
    DESCENT_AFTER_SEPERATION, //ayrılma sonrası alçalma
    STANDBY,                  //askıda
    LANDING                   //iniş
};

enum class MotionState_e {
    STEADYSTATE = 0x00, //askıda
    RISINGSTATE = 0x01,
    FALLINGSTATE = 0x02
};


#endif