#include "modeluydu2.hpp"

#include "motor.h"
#include "seperation.h"
#include "battery.h"
#include "gpio.h"
#include "pin_util.h"

#define MOTOR_TEST_TIME_MS 10000  // 10 seconds

//static uint32_t prev_time = 0;

ModelUydu2::ModelUydu2(GaziSensor* sensors) : 
    _gSensors(sensors),
    _stateMachine(StateId_e::LAUNCHPAD),
    _motor((uint8_t)HAL::Pin::MOTOR_PIN),
    _battery(HAL::Pin::BATTERY_PIN),
    _spCapsule(HAL::Pin::SEPERATION_SERVO_PIN),
    _spSmoke(HAL::Pin::FIRE_SMOKE_PIN),
    _spCamera(HAL::Pin::SEPERATION_CAMERA_PIN)  
{
    
}

void ModelUydu2::manuelSeperation()
{
    _spCapsule.run();
}

void ModelUydu2::manuelSmokeSperation()
{
    _spSmoke.run();
}

void ModelUydu2::manuelCameraSperation()
{
    _spCamera.run();
}

void ModelUydu2::manuelSperation(Seperation& seperator)
{
    seperator.run();
}

void ModelUydu2::automaticSeperation()
{
    if(_stateMachine.getState() == StateId_e::SEPERATION) {
        _spCapsule.safe_run();
    }
}

void ModelUydu2::manualImpellent()
{
    motorTestStart();
}

void ModelUydu2::controlEnergyMode()
{
    if(_battery.getStatus() == Battery::BatteryStatus::CRITICAL_FAILURE ) {

    }

}

void ModelUydu2::recoveryBuzzer(){
    if(_stateMachine.getState()==StateId_e::LANDING) {
        // TODO: add landing beeper
        //_buzz.beeper(Buzzer::LANDING)
    }
}

void ModelUydu2::motorControl(){
    if(_stateMachine.getState() == StateId_e::SEPERATION) {
        // TODO: wait 1s

        // TODO: chose one
        _motor.setSpeed(_velControlPID.compute(_gSensors->sensor.m_avg_velocity_z));
        //_motor.output();
    }
}

void ModelUydu2::testMotor() {
    _motor.motorTestOutput();
}

void ModelUydu2::activateSmoke()
{
    if(_stateMachine.getState() == StateId_e::SEPERATION) {
        _spSmoke.run();
    }
}

void ModelUydu2::sendTelemetry()
{

}

void ModelUydu2::stabilizationAltitude()
{
    
}

void ModelUydu2::pending_400_meters()
{

}

void ModelUydu2::motorTestStart()
{
    _motor.armed(true);
    _motor.motorTestOutput();
}