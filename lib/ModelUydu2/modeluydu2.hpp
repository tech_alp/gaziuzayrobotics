#ifndef _MODELUYDU_H
#define _MODELUYDU_H

#include "gazisensor.hpp"
#include "motor.h"
#include "battery.h"
#include "seperation.h"
#include "status_enum.h"
#include "gpio.h"
#include "state_machine.h"
#include "pid.h"
#include "buzzer.h"
#include "led.h"

class ModelUydu2
{
public:
    ModelUydu2(GaziSensor* sensors);

    enum MotorTestThrottle : uint8_t {
        MOTOR_TEST_THROTTLE_PERCENT_BY_INCREASING,
        MOTOR_TEST_THROTTLE_PWM,
        MOTOR_TEST_THROTTLE_PILOT
    };
    
    void init() {
        _spCapsule.begin();
        _spSmoke.begin();
        _spCamera.begin();
        _motor.init();
    }

    void manuelSeperation();
    void manuelSmokeSperation();
    void manuelCameraSperation();
    static void manuelSperation(Seperation& seperator);

    void automaticSeperation();

    void manualImpellent();

    void controlEnergyMode();

    void recoveryBuzzer();

    void motorControl();
    void testMotor();

    void activateSmoke();

    void sendTelemetry();

    void stabilizationAltitude();
    void pending_400_meters();

    void controlTelemetry();
    
    ServoSeperation& getServoSeperation() {
        return _spCapsule;
    }

    RelaySeperation& getSmokeSeperation() {
        return _spSmoke;
    }

private:
    GaziSensor* _gSensors;
    StateMachine _stateMachine;
    Motor _motor;
    Battery _battery;
    ServoSeperation _spCapsule;
    RelaySeperation _spSmoke;
    RelaySeperation _spCamera;
    Pid::PID _velControlPID = Pid::PID(0,0,0,0,0);
    
    void motorTestStart();
    void motorProcessCommand();
};

#endif