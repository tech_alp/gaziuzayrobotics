#ifndef SERIAL_MANAGER_H
#define SERIAL_MANAGER_H

#include <SoftwareSerial.h>
#include <HardwareSerial.h>

#include "config.h"

template<typename PORT>
class SerialManager {
public:

    SerialManager(PORT* port);

    /* Do not allow copy */
    SerialManager(const SerialManager&) = delete;
    SerialManager& operator=(const SerialManager&) = delete;

    void baudrate(long baud);

    void start(int baudrate);

    void setFlag(char value);

    void setDelimiter(char value);

    bool onReceive();

    String getValue();

    String getCmd();

    String getParam();

    bool isCmd(String value);

    bool isParam(String value);

    // ----------------------------------------------------------------------------
    template <typename T>
    void print(T value);

    // ----------------------------------------------------------------------------
    template <typename T>
    void println(T value);

private:      
    static SerialManager<PORT>* _smv;
    PORT* _port = nullptr;
    char _char;
    char _flag      = '\n'; // you must set the serial monitor to include a newline with each command
    char _delimiter = ',';
    String _buffer = "";
    String _value   = "";

    long _baudrate = 9600; //Default
};

typedef SerialManager<HardwareSerial> SerialMonitor_;
typedef SerialManager<SoftwareSerial> SerialSoftware_;

extern SerialMonitor_ SerialMonitor;

#endif