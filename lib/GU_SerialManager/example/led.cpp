#include <Arduino.h>
#include <SoftwareSerial.h>

#include <serialmanager.h>

#define LED_PIN 13

// CMD,PARAM
// Led,on  | Led,1
// Led,off | Led,0
// just "on"
// just "off"
// pwm,133

SerialMonitor_ usb(&Serial);
// SerialManager<HardwareSerial> usb(&Serial);
// SoftwareSerial myserial(5,6);
// SerialManager<SoftwareSerial> serial_manager2(&myserial);
// SerialSoftware serial_manager2(&myserial);

void setup()
{
    usb.start(9600);
    pinMode(LED_PIN,OUTPUT);
}

void loop()
{
    // you must set the serial monitor to include a newline with each command
    if(usb.onReceive()) {
        // example serial command: on
        if(usb.isCmd("on")) {
            digitalWrite(LED_PIN,HIGH);
        }

        // example serial command: off
        if(usb.isCmd("off")) {
            digitalWrite(LED_PIN,LOW);
        }

        // example serial command: pwm,123
        if(usb.isCmd("pwm")) {
          int pwm = usb.getParam().toInt();
          analogWrite(LED_PIN,pwm);
        }
  }
}