#include "serialmanager.h"

// ----------------------------------------------------------------------------
template<typename PORT>
SerialManager<PORT>* SerialManager<PORT>::_smv = nullptr;

// ----------------------------------------------------------------------------
template<typename PORT>
SerialManager<PORT>::SerialManager(PORT* port) : _port(port)
{
}

// ----------------------------------------------------------------------------
template<typename PORT>
void SerialManager<PORT>::baudrate(long baud)
{
    _baudrate = baud;
}

// ----------------------------------------------------------------------------
template<typename PORT>
void SerialManager<PORT>::start(int baudrate)
{
    _baudrate = baudrate;
    if(_port) {
        _port->begin(_baudrate);
    }
    else {
        DEBUG_PRINT("Serialport not created");
    }
}

// ----------------------------------------------------------------------------
template<typename PORT>
void SerialManager<PORT>::setFlag(char value) { _flag = value; }

// ----------------------------------------------------------------------------
template<typename PORT>
void SerialManager<PORT>::setDelimiter(char value) { _delimiter = value; }

// ----------------------------------------------------------------------------
template<typename PORT>
bool SerialManager<PORT>::onReceive() {
    while(_port->available()) {
      _char = char(_port->read());
      if(_char == _flag) {
        _value  = _buffer;
        _buffer = "";
        return true;
      }
      else {
        _buffer += _char;
        return false;
      }
    }
    
    return false;
}

// ----------------------------------------------------------------------------
template<typename PORT>
String SerialManager<PORT>::getValue()
{
    return _value;
}

// ----------------------------------------------------------------------------
template<typename PORT>
String SerialManager<PORT>::getCmd()
{
    int idx = _value.indexOf(_delimiter);
    if(idx > -1) {
        return _value.substring(0,idx);
    }
    else {
        return getValue();
    }
}

// ----------------------------------------------------------------------------
template<typename PORT>
String SerialManager<PORT>::getParam()
{
    int idx = getValue().indexOf(_delimiter);
    if(idx > -1) {
        return getValue().substring(idx+1,_value.length());
    }
    else {
        return "";
    }
}

// ----------------------------------------------------------------------------
template<typename PORT>
bool SerialManager<PORT>::isCmd(String value) 
{
    return getCmd() == value;
}

// ----------------------------------------------------------------------------
template<typename PORT>
bool SerialManager<PORT>::isParam(String value)
{
    return getParam() == value;
}

SerialMonitor_ SerialMonitor(&Serial);