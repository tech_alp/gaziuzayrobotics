#ifndef _MODELUDU_HPP
#define _MODELUDU_HPP

#include <Arduino.h>
#include "gaziuzay.hpp"

class Servo;

class ModelUydu : public GaziUzay {
public:
    ModelUydu();

    enum class TaskStatus {
        LAUNCHPAD,                //fırlatma rampası
        ASCENT,                   //yükseliş
        APOGEE,                   //zirve
        DESCENT,                  //alçalma
        SEPERATION,               //ayrılma
        DESCENT_AFTER_SEPERATION, //ayrılma sonrası alçalma
        STANDBY,                  //askıda
        LANDING                   //iniş
    };
    
    enum MotionState : uint8_t {
        STEADYSTATE, //sabit durum
        RISINGSTATE,
        FALLINGSTATE
    };

    enum MotorTestThrottle : uint8_t {
        MOTOR_TEST_THROTTLE_PERCENT_BY_INCREASING,
        MOTOR_TEST_THROTTLE_PWM,
        MOTOR_TEST_THROTTLE_PILOT
    };


    void manuelSperation();

    void automaticSeperation();

    void risingorfalling();

    int calibration();  // Calibration function for initial settings.

    void manualImpellent();

    void set_videoinfo();

    void recoveryBuzzer();

    void motorControl();

    void activateSmoke();

    const String& TelemetryPackage() const;

    void set_seperationServo(Servo* servo,uint8_t pin);

    void set_motorServo(Servo* servo,uint8_t pin);

    void motor_test_output();
    void motor_test_stop();
    void motor_test_start();
    
private:
    //GaziUzay* m_gaziUzay;
    Servo* m_seperationServo{nullptr};
    Servo* m_motorServo{nullptr};

    TaskStatus m_taskStatus{TaskStatus::LAUNCHPAD};
    MotionState m_motionStatus{MotionState::STEADYSTATE};
};

#endif