#include "modeluydu.hpp"

#include <Servo.h>

// motor test definitions
static const int16_t MOTOR_TEST_PWM_MAX = 2200; // max pwm value accepted by the test
static const int16_t MOTOR_TEST_TIMEOUT_MS_MAX = 10000; // max timeout is 10 seconds

static uint32_t motor_test_start_ms = 0;        // system time the motor test began
static uint32_t motor_test_timeout_ms = 0;      // test will timeout this many milliseconds after the motor_test_start_ms
static uint8_t motor_test_throttle_type = 0;    // motor throttle type (0=throttle percentage by increasing, 1=Constant PWM, 2=pilot throttle channel pass-through)
static int16_t motor_test_throttle_value = 0;   // throttle to be sent to motor, value depends upon it's type


ModelUydu::ModelUydu()
{

}

void ModelUydu::manuelSperation()
{
    m_seperationServo->write(180);
    m_taskStatus = TaskStatus::SEPERATION;
    // TODO: Test the motion status
    m_motionStatus = MotionState::FALLINGSTATE;
}

void ModelUydu::automaticSeperation()
{
    if(m_motionStatus==MotionState::FALLINGSTATE) {
        m_seperationServo->write(180);
        m_taskStatus = TaskStatus::SEPERATION;
    }
}

void ModelUydu::risingorfalling()
{

}

void ModelUydu::manualImpellent()
{
    //TODO: motor will work just 4 second
}

void ModelUydu::set_videoinfo()
{
    
}

void ModelUydu::recoveryBuzzer()
{
    
}

void ModelUydu::motorControl()
{

}

const String& ModelUydu::TelemetryPackage() const
{
    return  String (m_team_number) + "," + String (m_numofpackage) + "," + m_dateTime + "," + String (m_pressure) + "," + 
			String (m_altitude) + "," + String (m_velocity) + "," + String (m_temperature) + "," + String (m_voltage) + "," + 
			String (m_gps_latitude) + "," + String (m_gps_longitude) + "," + String (m_gps_altitude) + "," + String (m_statusSystem) + "," +
			String (m_pitch) + "," + String (m_roll) + "," + String (m_yaw) + "," + String (m_numofturns) + "," + String (m_videoInfo);
}

void ModelUydu::set_seperationServo(Servo* servo,uint8_t pin)
{
    m_seperationServo = servo;
    m_seperationServo->attach(pin);
}

void ModelUydu::set_motorServo(Servo* servo,uint8_t pin)
{
    m_motorServo = servo;
    m_motorServo->attach(pin);
}

// motor_test_output - checks for timeout and sends updates to motor object
void ModelUydu::motor_test_output() {
    if(!m_motorServo)
        return;

    // check for test timeout
    if ((millis() - motor_test_start_ms) >= motor_test_timeout_ms) {
        motor_test_stop();
    } else {// calculate  based on throttle type
        switch (motor_test_throttle_type) {
            case MOTOR_TEST_THROTTLE_PERCENT_BY_INCREASING:
                
            case MOTOR_TEST_THROTTLE_PWM:
                //TODO: how much need constant pwm?
                m_motorServo->write(600);
                break;

            case MOTOR_TEST_THROTTLE_PILOT:
                
                break;

            default:
                // do nothing
                return;
        }
    }
}

void ModelUydu::motor_test_stop() {
    if(!m_motorServo)
        return;
    
    m_motorServo->write(0);

    // reset timeout
    motor_test_start_ms = 0;
    motor_test_timeout_ms = 0;
}

void ModelUydu::motor_test_start() {
    
}