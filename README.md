## Content 
- [Gazi Uzay](#Gazi-Uzay)

    - [Team Advisor](#Team-Advisor)

    - [Team Members](#Team-Members)

    - [General Information About The Competition](#General-Information-About-The-Competition )

- [Software](#Software)

    - [Yer İst. - Pi - Teensy](#Yer-İst.---Pi---Teensy)

## Gazi Uzay

<p align="center" > 
<img src="logo/gazi.png" width="150" height="150">
</p>

> Gazi Uzay which founded in September 2019 finished 18th among 149 teams in the 2020 Teknofest Model Satellite Competition.


#### Team Advisor
- Prof.Dr. Ahmet Bingül

#### Team Members

- Enes Alp

- Tayfur Çınar

- Ozan Kaçar

- Tamer Köse

- Berkay Kaplan

- ~~Ahmet Uzun~~

- ~~Emre Can Sönmez~~

#### General Information About The Competition :

TÜRKSAT Model Satellite Competition is a design-build-flight competition. The T-MUY provides teams with an opportunity to experience process from design to beginning of active mission of an aerospace system. The T-MUY competition is planned to reflect an aerospace project on a small scale and includes all aspects of an aerospace project, from design to production and post-mission review. The competition is designed to reflect various aspects of real missions such as addressing telemetry and communication requirements, provide autonomous structure and developing an interdisciplinary working system.

T-MUY aims to provide undergraduate and graduate students with the opportunity to transfer knowledge from theory to practice and to acquiring the ability to work interdisciplinary. Additionally, It is aimed that the students will have the opportunity to share their experiences with other university teams and to communicate with the institution, companies, experts and engineers operating in the sector.

---

## Software 

<img src="logo/Communication_Design-1.jpg">

<br></br>

#### Yer İst. - Pi - Teensy
 
- Teensy 1 Hz olarak verileri Raspberry üzerine aktarırken aynı zamanda verileri SD karta yazacak. Buradaki SD bir nevi kara kutu olacak.

- Raspberry, Yer İstasyonundan gelen komutlar CMD olarak ayrıştırılacak.

- Veri Komutu geldiğinde Raspberry üzerine gelen verileri, Yer İstasyonuna iletecek ve aynı zamanda Raspberry üzerindeki 'dataTXT' dosyasına yazacak.

- Ayrılma Komutu geldiğinde Raspberry, komutu Teensy üzerine gönderecek ve sistem tetiklenecek.

- Tahrik Komutu geldiğinde  Raspberry, komutu Teensy üzerine gönderecek ve sistem tetiklenecek.

- Video Komutu geldiği zaman raspberry kendi SD kartına bu videoyu yazacak.

- Yer İstasyonu noktasındaki problemler çözülürse Kamera verisi Raspberry üzerinden gönderilebilir.

> İletişim protokolü bu senaryoda 802.11.b/g/n ile TCP/IP olacaktır.

**Çalışmamız gerekenler;**

1. TCP/IP Protokolü geliştirilmeli

2. Yer İstasyonu - Çift port okuması yapılmalı.

3. Teensy içerisinde Hız Verisi, GPS Verisi ve PID üzerine

<br></br>

<img src="logo/Communication_Design-2.jpg">

<br></br>

__*Yol #1*__

> Yazılım ve Haberleşme noktasında diyagramımız büyük bir ihtimalle böyle olacak.

__*Yol #2*__

> 2.yolda ise; FPV yerine RPI ile kamera görüntüsünü almak olacak.

- Basınç sensörünün doğrulluğu kontrol edilecek.

- Basınç sensöründen elde edilecek hız dönüşümü için algoritmalar araştırılacak.

- PID sistemi için dikey iniş sistemleri hakkında araştırmalar yapılacak.

- Basınç verisiyle elde edilen yükseklik verisinin doğruluğu kontrol edilecek.

- Model uydunun yükseliş ve alçalış durumlarında gerçekleşecek algoritmaların doğruluğu için testler hazırlanacak.

- Raspberry pi üzerinden kamera verileri transferi için araştırma yapılacak. 

- Basıç sensörü ile elde edilen 3 tane veri var ve bu veriler sistmein stabil çalışması için kritik veriler olduğu için yüksek doğrulukta olan basınç sensörü seçilecek. Basınç sensörü ile elde edilen verilen sırasıyla basınç, yükselik, hız verileridir.

#### Uçuş yazılım algoritmasını denemek için aşağıdaki testler uygulanmaladır.

1. Basınç verisi sensör üzerinden alınarak, kullanılacak barometre ile karşılaştırma yapılacaktır. 

<span style="color:darkred;">

2. Sistemin algoritmasını çalıştırabilmek için her zaman model uyduyu uçurayamayacağımız için bir yükseklik ve hız test dosyası yazılacak ve bu veriler yer istasyonundan model uyduya gönderilerek sanki sensörden veri alıyormuş gibi bir test yazılımı yazılacak ve bu test yazılımı üzerinden sistemin algoritmasının çalışıp çalışmadığı test edilecek.

</span>

3. Yazılan her algoritma ilgili sensörlerle test edildikten sonra sisteme eklenecek.